/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.services;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import org.pol.una.py.entities.NotaUs;
import org.pol.una.py.entities.NotaUsPK;

/**
 *
 * @author gustavo
 */
@Stateless
@Path("notasus")
public class NotaUsFacadeREST extends AbstractFacade<NotaUs> {

    @PersistenceContext(unitName = "WebApplication1PU")
    private EntityManager em;

    private NotaUsPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idNotaUs=idNotaUsValue;idUserStory=idUserStoryValue;idProyecto=idProyectoValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        org.pol.una.py.entities.NotaUsPK key = new org.pol.una.py.entities.NotaUsPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idNotaUs = map.get("idNotaUs");
        if (idNotaUs != null && !idNotaUs.isEmpty()) {
            key.setIdNotaUs(new java.lang.Integer(idNotaUs.get(0)));
        }
        java.util.List<String> idUserStory = map.get("idUserStory");
        if (idUserStory != null && !idUserStory.isEmpty()) {
            key.setIdUserStory(new java.lang.Integer(idUserStory.get(0)));
        }
        java.util.List<String> idProyecto = map.get("idProyecto");
        if (idProyecto != null && !idProyecto.isEmpty()) {
            key.setIdProyecto(new java.lang.Integer(idProyecto.get(0)));
        }
        return key;
    }

    public NotaUsFacadeREST() {
        super(NotaUs.class);
    }

    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(NotaUs entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") PathSegment id, NotaUs entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        org.pol.una.py.entities.NotaUsPK key = getPrimaryKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public NotaUs find(@PathParam("id") PathSegment id) {
        org.pol.una.py.entities.NotaUsPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<NotaUs> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<NotaUs> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
