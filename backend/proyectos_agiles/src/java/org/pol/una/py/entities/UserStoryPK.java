/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author gustavo
 */
@Embeddable
public class UserStoryPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_user_story")
    private int idUserStory;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_proyecto")
    private int idProyecto;

    public UserStoryPK() {
    }

    public UserStoryPK(int idUserStory, int idProyecto) {
        this.idUserStory = idUserStory;
        this.idProyecto = idProyecto;
    }

    public int getIdUserStory() {
        return idUserStory;
    }

    public void setIdUserStory(int idUserStory) {
        this.idUserStory = idUserStory;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }
    /*
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUserStory;
        hash += (int) idProyecto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserStoryPK)) {
            return false;
        }
        UserStoryPK other = (UserStoryPK) object;
        if (this.idUserStory != other.idUserStory) {
            return false;
        }
        if (this.idProyecto != other.idProyecto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.UserStoryPK[ idUserStory=" + idUserStory + ", idProyecto=" + idProyecto + " ]";
    }
    */
}
