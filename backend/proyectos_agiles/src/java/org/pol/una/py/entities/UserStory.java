/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gustavo
 */
@Entity
@Table(name = "user_story")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserStory.findAll", query = "SELECT u FROM UserStory u")
    , @NamedQuery(name = "UserStory.findByIdUserStory", query = "SELECT u FROM UserStory u WHERE u.userStoryPK.idUserStory = :idUserStory")
    , @NamedQuery(name = "UserStory.findBySprintNumber", query = "SELECT u FROM UserStory u WHERE u.sprint.sprintPK.idSprint = :idSprint and u.userStoryPK.idProyecto = :idProyecto")
    , @NamedQuery(name = "UserStory.findBacklog", query = "SELECT u FROM UserStory u WHERE u.idEstadoUs.idEstadoUs = 2 and u.userStoryPK.idProyecto = :idProyecto")
    , @NamedQuery(name = "UserStory.findByIdProyecto", query = "SELECT u FROM UserStory u WHERE u.userStoryPK.idProyecto = :idProyecto")
    , @NamedQuery(name = "UserStory.findByCodigoUserStory", query = "SELECT u FROM UserStory u WHERE u.codigoUserStory = :codigoUserStory")
    , @NamedQuery(name = "UserStory.findByIdUsuario", query = "SELECT u FROM UserStory u WHERE u.idUsuarioAsignadoUserStory.idUsuario = :idUsuarioAsignadoUserStory and  u.userStoryPK.idProyecto = :idProyecto")    
    , @NamedQuery(name = "UserStory.findByDescripcionUserStory", query = "SELECT u FROM UserStory u WHERE u.descripcionUserStory = :descripcionUserStory")
    , @NamedQuery(name = "UserStory.findByPrioridadUserStory", query = "SELECT u FROM UserStory u WHERE u.prioridadUserStory = :prioridadUserStory")
    , @NamedQuery(name = "UserStory.findByTiempoPlanificadoUserStory", query = "SELECT u FROM UserStory u WHERE u.tiempoPlanificadoUserStory = :tiempoPlanificadoUserStory")
    , @NamedQuery(name = "UserStory.findByTiempoEjecutadoUserStory", query = "SELECT u FROM UserStory u WHERE u.tiempoEjecutadoUserStory = :tiempoEjecutadoUserStory")
    , @NamedQuery(name = "UserStory.findByFechaCreacionUserStory", query = "SELECT u FROM UserStory u WHERE u.fechaCreacionUserStory = :fechaCreacionUserStory")
    , @NamedQuery(name = "UserStory.findByFechaFinUserStory", query = "SELECT u FROM UserStory u WHERE u.fechaFinUserStory = :fechaFinUserStory")
    , @NamedQuery(name = "UserStory.findByNombreUs", query = "SELECT u FROM UserStory u WHERE u.nombreUs = :nombreUs")})
public class UserStory implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserStoryPK userStoryPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "codigo_user_story")
    private String codigoUserStory;
    @Size(max = 200)
    @Column(name = "descripcion_user_story")
    private String descripcionUserStory;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prioridad_user_story")
    private int prioridadUserStory;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tiempo_planificado_user_story")
    private int tiempoPlanificadoUserStory;
    @Column(name = "tiempo_ejecutado_user_story")
    private Integer tiempoEjecutadoUserStory;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion_user_story")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionUserStory;
    @Column(name = "fecha_fin_user_story")
    @Temporal(TemporalType.DATE)
    private Date fechaFinUserStory;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombre_us")
    private String nombreUs;
    @JoinColumn(name = "id_estado_us", referencedColumnName = "id_estado_us")
    @ManyToOne(optional = false)
    private EstadoUs idEstadoUs;
    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Proyecto proyecto;
    @JoinColumns({
        @JoinColumn(name = "id_sprint", referencedColumnName = "id_sprint")
        , @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Sprint sprint;
    @JoinColumn(name = "id_tipo_us", referencedColumnName = "id_tipo_us")
    @ManyToOne(optional = false)
    private TipoUs idTipoUs;
    @JoinColumn(name = "id_usuario_creacion_user_story", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuario idUsuarioCreacionUserStory;
    @JoinColumn(name = "id_usuario_asignado_user_story", referencedColumnName = "id_usuario")
    @ManyToOne(optional = false)
    private Usuario idUsuarioAsignadoUserStory;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userStory")
    private Collection<NotaUs> notaUsCollection;

    public UserStory() {
    }

    public UserStory(UserStoryPK userStoryPK) {
        this.userStoryPK = userStoryPK;
    }

    public UserStory(UserStoryPK userStoryPK, String codigoUserStory, int prioridadUserStory, int tiempoPlanificadoUserStory, Date fechaCreacionUserStory, String nombreUs) {
        this.userStoryPK = userStoryPK;
        this.codigoUserStory = codigoUserStory;
        this.prioridadUserStory = prioridadUserStory;
        this.tiempoPlanificadoUserStory = tiempoPlanificadoUserStory;
        this.fechaCreacionUserStory = fechaCreacionUserStory;
        this.nombreUs = nombreUs;
    }

    public UserStory(int idUserStory, int idProyecto) {
        this.userStoryPK = new UserStoryPK(idUserStory, idProyecto);
    }

    public UserStoryPK getUserStoryPK() {
        return userStoryPK;
    }

    public void setUserStoryPK(UserStoryPK userStoryPK) {
        this.userStoryPK = userStoryPK;
    }

    public String getCodigoUserStory() {
        return codigoUserStory;
    }

    public void setCodigoUserStory(String codigoUserStory) {
        this.codigoUserStory = codigoUserStory;
    }

    public String getDescripcionUserStory() {
        return descripcionUserStory;
    }

    public void setDescripcionUserStory(String descripcionUserStory) {
        this.descripcionUserStory = descripcionUserStory;
    }

    public int getPrioridadUserStory() {
        return prioridadUserStory;
    }

    public void setPrioridadUserStory(int prioridadUserStory) {
        this.prioridadUserStory = prioridadUserStory;
    }

    public int getTiempoPlanificadoUserStory() {
        return tiempoPlanificadoUserStory;
    }

    public void setTiempoPlanificadoUserStory(int tiempoPlanificadoUserStory) {
        this.tiempoPlanificadoUserStory = tiempoPlanificadoUserStory;
    }

    public Integer getTiempoEjecutadoUserStory() {
        return tiempoEjecutadoUserStory;
    }

    public void setTiempoEjecutadoUserStory(Integer tiempoEjecutadoUserStory) {
        this.tiempoEjecutadoUserStory = tiempoEjecutadoUserStory;
    }

    public Date getFechaCreacionUserStory() {
        return fechaCreacionUserStory;
    }

    public void setFechaCreacionUserStory(Date fechaCreacionUserStory) {
        this.fechaCreacionUserStory = fechaCreacionUserStory;
    }

    public Date getFechaFinUserStory() {
        return fechaFinUserStory;
    }

    public void setFechaFinUserStory(Date fechaFinUserStory) {
        this.fechaFinUserStory = fechaFinUserStory;
    }

    public String getNombreUs() {
        return nombreUs;
    }

    public void setNombreUs(String nombreUs) {
        this.nombreUs = nombreUs;
    }

    

    public void setIdEstadoUs(EstadoUs idEstadoUs) {
        this.idEstadoUs = idEstadoUs;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    

    public void setIdTipoUs(TipoUs idTipoUs) {
        this.idTipoUs = idTipoUs;
    }

    

    public void setIdUsuarioCreacionUserStory(Usuario idUsuarioCreacionUserStory) {
        this.idUsuarioCreacionUserStory = idUsuarioCreacionUserStory;
    }

    

    public void setIdUsuarioAsignadoUserStory(Usuario idUsuarioAsignadoUserStory) {
        this.idUsuarioAsignadoUserStory = idUsuarioAsignadoUserStory;
    }

    @XmlTransient
    public Collection<NotaUs> getNotaUsCollection() {
        return notaUsCollection;
    }

    public void setNotaUsCollection(Collection<NotaUs> notaUsCollection) {
        this.notaUsCollection = notaUsCollection;
    }
    /*
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userStoryPK != null ? userStoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserStory)) {
            return false;
        }
        UserStory other = (UserStory) object;
        if ((this.userStoryPK == null && other.userStoryPK != null) || (this.userStoryPK != null && !this.userStoryPK.equals(other.userStoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.UserStory[ userStoryPK=" + userStoryPK + " ]";
    }
    */
}
