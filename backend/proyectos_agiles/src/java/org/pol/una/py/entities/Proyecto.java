/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
/**
 *
 * @author gustavo
 */
@Entity
@Table(name = "proyecto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proyecto.findAll", query = "SELECT p FROM Proyecto p")
    , @NamedQuery(name = "Proyecto.findByIdUsuario", query = "SELECT p FROM Proyecto p JOIN FETCH p.usuarioProyectoCollection up WHERE up.usuario.idUsuario = :idUsuario")    
    , @NamedQuery(name = "Proyecto.findByIdProyecto", query = "SELECT p FROM Proyecto p WHERE p.idProyecto = :idProyecto")
    , @NamedQuery(name = "Proyecto.findByCodigoProyecto", query = "SELECT p FROM Proyecto p WHERE p.codigoProyecto = :codigoProyecto")
    , @NamedQuery(name = "Proyecto.findByNombreProyecto", query = "SELECT p FROM Proyecto p WHERE p.nombreProyecto = :nombreProyecto")
    , @NamedQuery(name = "Proyecto.findByFechaInicioProyecto", query = "SELECT p FROM Proyecto p WHERE p.fechaInicioProyecto = :fechaInicioProyecto")
    , @NamedQuery(name = "Proyecto.findByFechaFinProyecto", query = "SELECT p FROM Proyecto p WHERE p.fechaFinProyecto = :fechaFinProyecto")
    , @NamedQuery(name = "Proyecto.findByDuracionSprintsProyecto", query = "SELECT p FROM Proyecto p WHERE p.duracionSprintsProyecto = :duracionSprintsProyecto")})
public class Proyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proyecto")
    private Integer idProyecto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "codigo_proyecto")
    private String codigoProyecto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre_proyecto")
    private String nombreProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio_proyecto")
    @Temporal(TemporalType.DATE)
    private Date fechaInicioProyecto;
    @Column(name = "fecha_fin_proyecto")
    @Temporal(TemporalType.DATE)
    private Date fechaFinProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "duracion_sprints_proyecto")
    private int duracionSprintsProyecto;
    @JoinTable(name = "tipo_us_proyecto", joinColumns = {
        @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto")}, inverseJoinColumns = {
        @JoinColumn(name = "id_tipo_us", referencedColumnName = "id_tipo_us")})
    @ManyToMany(cascade = CascadeType.ALL)
    private Collection<TipoUs> tipoUsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private Collection<UserStory> userStoryCollection;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name="proyecto_usuario", joinColumns =@JoinColumn(name="id_proyecto"), inverseJoinColumns =@JoinColumn(name="id_usuario_proyecto") )
    private Collection<UsuarioProyecto> usuarioProyectoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyecto")
    private Collection<Sprint> sprintCollection;
    @JoinColumn(name = "id_estado_proyecto", referencedColumnName = "id_estado_proyecto")
    @ManyToOne()
    private EstadoProyecto idEstadoProyecto;

    public Proyecto() {
    }

    public Proyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Proyecto(Integer idProyecto, String nombreProyecto,String codigoProyecto, Date fechaInicioProyecto, int duracionSprintsProyecto) {
        this.idProyecto = idProyecto;
        this.codigoProyecto = codigoProyecto;
        this.nombreProyecto = nombreProyecto;
        this.fechaInicioProyecto = fechaInicioProyecto;
        this.duracionSprintsProyecto = duracionSprintsProyecto;
    }

    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getCodigoProyecto() {
        return codigoProyecto;
    }

    public void setCodigoProyecto(String codigoProyecto) {
        this.codigoProyecto = codigoProyecto;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public Date getFechaInicioProyecto() {
        return new Date(fechaInicioProyecto.getTime());
    }

    public void setFechaInicioProyecto(long fechaInicioProyecto) {
        this.fechaInicioProyecto = new Date(fechaInicioProyecto);
    }

    public Date getFechaFinProyecto() {
        return new Date(fechaFinProyecto.getTime());
    }

    public void setFechaFinProyecto(long fechaFinProyecto) {
        this.fechaFinProyecto = new Date(fechaFinProyecto);
    }

    public int getDuracionSprintsProyecto() {
        return duracionSprintsProyecto;
    }

    public void setDuracionSprintsProyecto(int duracionSprintsProyecto) {
        this.duracionSprintsProyecto = duracionSprintsProyecto;
    }

    @XmlTransient
    public Collection<TipoUs> getTipoUsCollection() {
        return tipoUsCollection;
    }

    public void setTipoUsCollection(Collection<TipoUs> tipoUsCollection) {
        this.tipoUsCollection = tipoUsCollection;
    }

    @XmlTransient
    public Collection<UserStory> getUserStoryCollection() {
        return userStoryCollection;
    }

    public void setUserStoryCollection(Collection<UserStory> userStoryCollection) {
        this.userStoryCollection = userStoryCollection;
    }

    @XmlTransient
    public Collection<UsuarioProyecto> getUsuarioProyectoCollection() {
        return usuarioProyectoCollection;
    }

    public void setUsuarioProyectoCollection(Collection<UsuarioProyecto> usuarioProyectoCollection) {
        this.usuarioProyectoCollection = usuarioProyectoCollection;
    }

    @XmlTransient
    public Collection<Sprint> getSprintCollection() {
        return sprintCollection;
    }

    public void setSprintCollection(Collection<Sprint> sprintCollection) {
        this.sprintCollection = sprintCollection;
    }

    public EstadoProyecto getIdEstadoProyecto() {
        return idEstadoProyecto;
    }

    public void setIdEstadoProyecto(EstadoProyecto idEstadoProyecto) {
        this.idEstadoProyecto = idEstadoProyecto;
    }

    /*@Override
    public int hashCode() {
        int hash = 0;
        hash += (idProyecto != null ? idProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.idProyecto == null && other.idProyecto != null) || (this.idProyecto != null && !this.idProyecto.equals(other.idProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.Proyecto[ idProyecto=" + idProyecto + " ]";
    }*/
    
}
