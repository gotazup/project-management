package com.example.usuario.gestionproyectos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MisTareasPorHacer extends Fragment {

    private RecyclerView tareas_por_hacer;
    private RecyclerView.Adapter adapter;
    private Usuario usuario;

    public void MisTareasPorHacer(Usuario usuario){
        this.usuario = usuario;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mis_tareas_por_hacer, container, false);

        Bundle args = getArguments();
        usuario = (Usuario) args.getSerializable("usuario");

        ArrayList<UserStory> tareas_por_hacer = initMisTareasPorHacer();
        this.tareas_por_hacer =(RecyclerView) view.findViewById(R.id.tareas_por_hacer);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext().getApplicationContext());
        this.tareas_por_hacer.setLayoutManager(mLayoutManager);

        adapter = new MisTareasAdapter(tareas_por_hacer,getContext().getApplicationContext(), usuario);
        this.tareas_por_hacer.setAdapter(adapter);

        return view;
    }

    private ArrayList<UserStory> initMisTareasPorHacer(){
        ArrayList<UserStory> list = new ArrayList<>();

        list.add(new UserStory("PR1-1", "Agregar botón guardar", new TipoUs("Desarrollo"),"Botón guardar en vista creación de tareas",1));
        list.add(new UserStory("PR1-2", "Agregar botón volver", new TipoUs("Desarrollo"),"Botón volver en vista creación de tareas",2));
        list.add(new UserStory("PR1-3", "Probar funcionalidades del módulo UsuarioList", new TipoUs("Testing"),"Probar ABM",3));

        return list;
    }
}
