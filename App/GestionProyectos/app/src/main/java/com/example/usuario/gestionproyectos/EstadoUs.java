package com.example.usuario.gestionproyectos;

import java.io.Serializable;
import java.util.Collection;

public class EstadoUs implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idEstadoUs;

    private String descripcionEstado;

    private Collection<UserStory> userStoryCollection;

    public EstadoUs() {
    }

    public EstadoUs(Integer idEstadoUs) {
        this.idEstadoUs = idEstadoUs;
    }

    public EstadoUs(Integer idEstadoUs, String descripcionEstado) {
        this.idEstadoUs = idEstadoUs;
        this.descripcionEstado = descripcionEstado;
    }

    public Integer getIdEstadoUs() {
        return idEstadoUs;
    }

    public void setIdEstadoUs(Integer idEstadoUs) {
        this.idEstadoUs = idEstadoUs;
    }

    public String getDescripcionEstado() {
        return descripcionEstado;
    }

    public void setDescripcionEstado(String descripcionEstado) {
        this.descripcionEstado = descripcionEstado;
    }

    public Collection<UserStory> getUserStoryCollection() {
        return userStoryCollection;
    }

    public void setUserStoryCollection(Collection<UserStory> userStoryCollection) {
        this.userStoryCollection = userStoryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstadoUs != null ? idEstadoUs.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoUs)) {
            return false;
        }
        EstadoUs other = (EstadoUs) object;
        if ((this.idEstadoUs == null && other.idEstadoUs != null) || (this.idEstadoUs != null && !this.idEstadoUs.equals(other.idEstadoUs))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.EstadoUs[ idEstadoUs=" + idEstadoUs + " ]";
    }

}
