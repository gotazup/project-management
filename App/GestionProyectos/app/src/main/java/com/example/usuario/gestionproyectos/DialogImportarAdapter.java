package com.example.usuario.gestionproyectos;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class DialogImportarAdapter extends ArrayAdapter<Proyecto> implements View.OnClickListener{

    private ArrayList<Proyecto> dataSet;
    Context mContext;
    private ArrayList<Boolean> itemChecked = new ArrayList<Boolean>();

    // View lookup cache
    private static class ViewHolder {
        TextView nombre;
        CheckBox importar;
    }

    public DialogImportarAdapter(ArrayList<Proyecto> data, Context context) {
        super(context, R.layout.list_item_dialog_importar, data);
        this.dataSet = data;
        this.mContext=context;

        for (int i = 0; i < this.getCount(); i++) {
            itemChecked.add(i, false); // initializes all items value with false
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return dataSet.size();
    }

    @Override
    public Proyecto getItem(int position) {
        // TODO Auto-generated method stub
        return dataSet.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        Proyecto dataModel=(Proyecto) object;


    }

    private int lastPosition = -1;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Proyecto dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_dialog_importar, parent, false);
            viewHolder.nombre = (TextView) convertView.findViewById(R.id.importar_nombre_proyecto);
            viewHolder.importar = (CheckBox) convertView.findViewById(R.id.importar_proyecto_tipos);
            viewHolder.importar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "asdsfdsfsf", Toast.LENGTH_SHORT).show();
                }
            });

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }



        //Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        //result.startAnimation(animation);
        //lastPosition = position;

        viewHolder.nombre.setText(dataModel.getNombreProyecto());

        // Return the completed view to render on screen
        return convertView;
    }
}