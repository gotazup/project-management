package com.example.usuario.gestionproyectos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

public class Sprint implements Serializable{

    private static final long serialVersionUID = 1L;

    protected SprintPK sprintPK;

    private String nombreSprint;

    private String estadoSprint;

    private long fechaCreacionSprint;

    private long fechaFinSprint;

    private Collection<UserStory> userStoryCollection;

    private Proyecto proyecto;

    private String codigoSprint;


    public Sprint(SprintPK sprintPK) {
        this.sprintPK = sprintPK;
    }

    public Sprint(SprintPK sprintPK, String nombreSprint, String estadoSprint, long fechaCreacionSprint) {
        this.sprintPK = sprintPK;
        this.nombreSprint = nombreSprint;
        this.estadoSprint = estadoSprint;
        this.fechaCreacionSprint = fechaCreacionSprint;
    }

    public Sprint(){
    }

    public Sprint(String codigoSprint, String nombreSprint, long fechaCreacionSprint, long fechaFinSprint){
        this.nombreSprint = nombreSprint;
        this.estadoSprint = "PE";
        this.fechaCreacionSprint = fechaCreacionSprint;
        this.fechaFinSprint = fechaFinSprint;
        this.codigoSprint = codigoSprint;
    }

    public Sprint(int idSprint, int idProyecto) {
        this.sprintPK = new SprintPK(idSprint, idProyecto);
    }

    public SprintPK getSprintPK() {
        return sprintPK;
    }

    public void setSprintPK(SprintPK sprintPK) {
        this.sprintPK = sprintPK;
    }

    public String getNombreSprint() {
        return nombreSprint;
    }

    public void setNombreSprint(String nombreSprint) {
        this.nombreSprint = nombreSprint;
    }

    public String getEstadoSprint() {
        return estadoSprint;
    }

    public void setEstadoSprint(String estadoSprint) {
        this.estadoSprint = estadoSprint;
    }

    public long getFechaCreacionSprint() {
        return fechaCreacionSprint;
    }

    public void setFechaCreacionSprint(long fechaCreacionSprint) {
        this.fechaCreacionSprint = fechaCreacionSprint;
    }

    public long getFechaFinSprint() {
        return fechaFinSprint;
    }

    public void setFechaFinSprint(long fechaFinSprint) {
        this.fechaFinSprint = fechaFinSprint;
    }

    public Collection<UserStory> getUserStoryCollection() {
        return userStoryCollection;
    }

    public void setUserStoryCollection(Collection<UserStory> userStoryCollection) {
        this.userStoryCollection = userStoryCollection;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public String getCodigoSprint() {
        return codigoSprint;
    }

    public void setCodigoSprint(String codigoSprint) {
        this.codigoSprint = codigoSprint;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sprintPK != null ? sprintPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sprint)) {
            return false;
        }
        Sprint other = (Sprint) object;
        if ((this.sprintPK == null && other.sprintPK != null) || (this.sprintPK != null && !this.sprintPK.equals(other.sprintPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.Sprint[ sprintPK=" + sprintPK + " ]";
    }

}
