package com.example.usuario.gestionproyectos;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SprintAddAdapter extends RecyclerView.Adapter<SprintAddAdapter.ViewHolder> {
    private ArrayList<UserStory> tareas;
    private Context mCtx;

    public SprintAddAdapter(ArrayList<UserStory> tareas, Context mCtx) {
        this.tareas = tareas;
        this.mCtx = mCtx;
    }


    @Override
    public SprintAddAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_add_tarea_sprint, parent, false);

        return new SprintAddAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SprintAddAdapter.ViewHolder holder, final int position) {
        UserStory tarea = tareas.get(position);

        int tipoPrioridad =  tarea.getPrioridadUserStory();
        switch (tipoPrioridad) {
            case 1:
                holder.prioridad.setImageResource(R.drawable.ic_arrow_upward_red);
                break;
            case 2:
                holder.prioridad.setImageResource(R.drawable.ic_arrow_upward_orange);
                break;
            case 3:
                holder.prioridad.setImageResource(R.drawable.ic_arrow_upward_green);
                break;
        }
        holder.codigo.setText(tarea.getCodigoUserStory());
        holder.nombre.setText(tarea.getNombreUs());

    }

    @Override
    public int getItemCount() {
        if (tareas != null) {
            return tareas.size();
        } else {
            return 0;
        }
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        public final View view;
        public final ImageView prioridad;
        public final TextView codigo;
        public final TextView nombre;
        public final CheckBox add;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            prioridad = view.findViewById(R.id.list_prioridad_tarea_sprint);
            codigo = view.findViewById(R.id.list_codigo_tarea_sprint);
            nombre = view.findViewById(R.id.list_nombre_tarea_sprint);
            add = view.findViewById(R.id.add_tarea_sprint);
        }
    }
}
