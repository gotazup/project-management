package com.example.usuario.gestionproyectos;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TareaUpd extends AppCompatActivity {

    TextView codigo, estado, nombre, descripcion, prioridad, planificado, ejecutado, fecha_fin, tipo, usuario, sprint;
    UserStory tarea;
    RestServices restServices;
    ArrayList<EstadoUs> listEstados = new ArrayList<EstadoUs>();
    private LinearLayout parentLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarea_upd);
        restServices = ApiUtils.getRestService();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        tarea = (UserStory) extras.getSerializable("tarea");
        cargar(tarea);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_guardar:
                guardar();
                return true;
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void cargar(UserStory tarea){
        codigo = (TextView) findViewById(R.id.upd_codigo_tarea);
        codigo.setText(tarea.getCodigoUserStory());
        nombre = (TextView) findViewById(R.id.upd_nombre_tarea);
        nombre.setText(tarea.getNombreUs());
        descripcion = (TextView) findViewById(R.id.upd_descripcion_tarea);
        descripcion.setText(tarea.getDescripcionUserStory());
        planificado = (TextView) findViewById(R.id.upd_tiempo_planificado_tarea);
        planificado.setText(tarea.getTiempoPlanificadoUserStory());
        ejecutado = (TextView) findViewById(R.id.upd_tiempo_ejecutado_tarea);
        ejecutado.setText(tarea.getTiempoEjecutadoUserStory());
        fecha_fin = (TextView) findViewById(R.id.upd_fecha_fin_tarea);
        fecha_fin.setText(Long.toString(tarea.getFechaFinUserStory()));
        tipo = (TextView) findViewById(R.id.upd_tipo_tarea);
        tipo.setText("");
        usuario = (TextView) findViewById(R.id.upd_usuario_asignado_tarea);
        usuario.setText("");
        if(tarea.getSprint()!=null) {
            sprint = (TextView) findViewById(R.id.upd_sprint_tarea);
            sprint.setText(tarea.getSprint().getCodigoSprint());
        }
        getEstados();
        estado = (TextView) findViewById(R.id.upd_estado_tarea);
        estado.setText(tarea.getIdEstadoUs().getDescripcionEstado());
        getPrioridades();
        prioridad = (TextView) findViewById(R.id.upd_prioridad_tarea);
        if(tarea.getPrioridadUserStory()==1){
            prioridad.setText("Baja");
        }else if(tarea.getPrioridadUserStory()==2){
            prioridad.setText("Media");
        }else{
            prioridad.setText("Alta");
        }


    }

    public void guardar(){
        codigo = (TextView) findViewById(R.id.upd_codigo_tarea);
        estado = (TextView) findViewById(R.id.upd_estado_tarea);
        nombre = (TextView) findViewById(R.id.upd_nombre_tarea);
        descripcion = (TextView) findViewById(R.id.upd_descripcion_tarea);
        prioridad = (TextView) findViewById(R.id.upd_prioridad_tarea);
        planificado = (TextView) findViewById(R.id.upd_tiempo_planificado_tarea);
        ejecutado = (TextView) findViewById(R.id.upd_tiempo_ejecutado_tarea);
        fecha_fin = (TextView) findViewById(R.id.upd_fecha_fin_tarea);
        tipo = (TextView) findViewById(R.id.upd_tipo_tarea);
        usuario = (TextView) findViewById(R.id.upd_usuario_asignado_tarea);
        sprint = (TextView) findViewById(R.id.upd_sprint_tarea);

        //WS guardar tarea
    }

    public void getEstados(){
        Call<ArrayList<EstadoUs>> call = restServices.getEstadoUS();
        call.enqueue(new Callback<ArrayList<EstadoUs>>(){
            @Override
            public void onResponse(Call<ArrayList<EstadoUs>> call, Response<ArrayList<EstadoUs>> response) {
                RecyclerView estados;


                if(response.isSuccessful()){
                    listEstados = response.body();

                    ArrayAdapter<EstadoUs> adapter = new ArrayAdapter<EstadoUs>(TareaUpd.this,
                            android.R.layout.simple_dropdown_item_1line, listEstados);
                    MaterialBetterSpinner betterSpinner = findViewById(R.id.upd_estado_tarea);
                    betterSpinner.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<EstadoUs>> call, Throwable t) {
                Toast.makeText(TareaUpd.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getPrioridades(){
        String[] prioridades = new String[]{"Baja","Media","Alta"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(TareaUpd.this,
                android.R.layout.simple_dropdown_item_1line, prioridades);
        MaterialBetterSpinner betterSpinner = findViewById(R.id.upd_prioridad_tarea);
        betterSpinner.setAdapter(adapter);
    }

    public void onAddNota(View v) {
        parentLinearLayout = (LinearLayout) findViewById(R.id.add_notas);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.add_nota_tarea, null);
        // Add the new row before the add field button.
        parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
    }

    public void onDelete(View v) {
        parentLinearLayout.removeView((View) v.getParent());
    }
}
