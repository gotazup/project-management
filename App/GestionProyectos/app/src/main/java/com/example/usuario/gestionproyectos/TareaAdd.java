package com.example.usuario.gestionproyectos;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TareaAdd extends AppCompatActivity {

    TextView codigo, nombre, descripcion, planificado, ejecutado, fecha_fin;
    MaterialBetterSpinner prioridad, tipo, usuario_asignado;
    RestServices restServices;
    ArrayList<TipoUs> listTipos = new ArrayList<TipoUs>();
    ArrayList<String> tiposNames = new ArrayList<String>();
    ArrayList<Usuario> listUsuarios = new ArrayList<Usuario>();
    ArrayList<String> userNames = new ArrayList<String>();
    Usuario usuario;
    Proyecto proyecto;

    private LinearLayout parentLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tarea_add);
        restServices = ApiUtils.getRestService();

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("usuario");
        proyecto = (Proyecto) extras.getSerializable("proyecto");

        getPrioridades();
        getTipos(proyecto);
        getUsuarios(proyecto);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_guardar:
                guardar();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void guardar(){
        codigo = (TextView) findViewById(R.id.add_codigo_tarea);
        nombre = (TextView) findViewById(R.id.add_nombre_tarea);
        descripcion = (TextView) findViewById(R.id.add_descripcion_tarea);
        prioridad = (MaterialBetterSpinner) findViewById(R.id.add_prioridad_tarea);
        planificado = (TextView) findViewById(R.id.add_tiempo_planificado_tarea);
        ejecutado = (TextView) findViewById(R.id.add_tiempo_ejecutado_tarea);
        fecha_fin = (TextView) findViewById(R.id.add_fecha_fin_tarea);
        tipo = (MaterialBetterSpinner) findViewById(R.id.add_tipo_tarea);
        usuario_asignado = (MaterialBetterSpinner) findViewById(R.id.add_usuario_asignado_tarea);

        int prioridad_int;
        if(prioridad.getText().toString()=="Alta"){
            prioridad_int = 1;
        }else if(prioridad.getText().toString()=="Media"){
            prioridad_int = 2;
        }else{
            prioridad_int = 3;
        }

        long endDate=0;
        try {
            String dateString = fecha_fin.getText().toString();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(dateString);

            endDate = date.getTime()/1000;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        TipoUs tipo_us=null;
        for (int i = 0; i < listTipos.size(); i++) {
            if(tipo.getText().toString()==listTipos.get(i).getNombreTipoUs()){
                tipo_us = listTipos.get(i);
                break;
            }
        }

        Usuario usuario_us=null;
        for (int i = 0; i < listUsuarios.size(); i++) {
            if(usuario_asignado.getText().toString()==listUsuarios.get(i).getNombreUsuario()){
                usuario_us = listUsuarios.get(i);
                break;
            }
        }

        ArrayList<NotaUs> listNotas = new ArrayList<NotaUs>();

        LinearLayout layout = findViewById(R.id.add_notas);
        Integer count = layout.getChildCount();
        View v = null;
        for(int i=0; i<(count-1); i++) {
            v = layout.getChildAt(i);
            LinearLayout linear = (LinearLayout) v;
            View tv = linear.getChildAt(1);
            TextInputLayout textILayout = (TextInputLayout) tv;
            View til = textILayout.getChildAt(0);
            TextInputEditText textEdit = (TextInputEditText) ((FrameLayout) til).getChildAt(0);

            listNotas.add(new NotaUs(textEdit.getText().toString(), (((new Date()).getTime())/1000), usuario));
        }

        UserStory tarea = new UserStory();
        tarea.setCodigoUserStory(codigo.getText().toString());
        tarea.setNombreUs(nombre.getText().toString());
        tarea.setDescripcionUserStory(descripcion.getText().toString());
        tarea.setPrioridadUserStory(prioridad_int);
        tarea.setTiempoPlanificadoUserStory(Integer.parseInt(planificado.getText().toString()));
        tarea.setTiempoEjecutadoUserStory(Integer.parseInt(ejecutado.getText().toString()));
        tarea.setFechaCreacionUserStory(((new Date()).getTime())/1000);
        tarea.setFechaFinUserStory(endDate);
        tarea.setIdTipoUs(tipo_us);
        tarea.setIdUsuarioAsignadoUserStory(usuario_us);
        tarea.setNotaUsCollection(listNotas);

        guardarTarea(tarea);
    }

    public void guardarTarea(UserStory tarea){
        restServices = ApiUtils.getRestService();
        Call<UserStory> call = restServices.addUserStory(tarea);
        call.enqueue(new Callback<UserStory>(){
            @Override
            public void onResponse(Call<UserStory> call, Response<UserStory> response) {
                if(response.isSuccessful()){
                    Toast.makeText(TareaAdd.this, "Tarea creada correctamente!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserStory> call, Throwable t) {
                Toast.makeText(TareaAdd.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getPrioridades(){
        String[] prioridades = new String[]{"Baja","Media","Alta"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(TareaAdd.this,
                android.R.layout.simple_dropdown_item_1line, prioridades);
        MaterialBetterSpinner betterSpinner = findViewById(R.id.add_prioridad_tarea);
        betterSpinner.setAdapter(adapter);
    }

    public void getTipos(Proyecto proyecto){
        Call<ArrayList<TipoUs>> call = restServices.getTiposPorProyecto(proyecto.getIdProyecto());
        call.enqueue(new Callback<ArrayList<TipoUs>>(){
            @Override
            public void onResponse(Call<ArrayList<TipoUs>> call, Response<ArrayList<TipoUs>> response) {
                if(response.isSuccessful()){
                    listTipos = response.body();

                    for(int i=0;i<listTipos.size();i++){
                        tiposNames.add(listTipos.get(i).getNombreTipoUs());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(TareaAdd.this,
                            android.R.layout.simple_dropdown_item_1line, tiposNames);
                    MaterialBetterSpinner betterSpinner = findViewById(R.id.add_tipo_tarea);
                    betterSpinner.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TipoUs>> call, Throwable t) {
                Toast.makeText(TareaAdd.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getUsuarios(Proyecto proyecto){
        Call<ArrayList<Usuario>> call = restServices.getUsuariosPorProyecto(proyecto.getIdProyecto());
        call.enqueue(new Callback<ArrayList<Usuario>>(){
            @Override
            public void onResponse(Call<ArrayList<Usuario>> call, Response<ArrayList<Usuario>> response) {
                if(response.isSuccessful()){
                    listUsuarios = response.body();

                    for(int i=0;i<listUsuarios.size();i++){
                        userNames.add(listUsuarios.get(i).getNombreUsuario());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(TareaAdd.this,
                            android.R.layout.simple_dropdown_item_1line, userNames);
                    MaterialBetterSpinner betterSpinner = findViewById(R.id.add_usuario_asignado_tarea);
                    betterSpinner.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Usuario>> call, Throwable t) {
                Toast.makeText(TareaAdd.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onAddNota(View v) {
        parentLinearLayout = (LinearLayout) findViewById(R.id.add_notas);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.add_nota_tarea, null);
        // Add the new row before the add field button.
        parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
    }

    public void onDelete(View v) {
        parentLinearLayout.removeView((View) v.getParent());
    }
}
