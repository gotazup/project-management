package com.example.usuario.gestionproyectos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class SprintsAdapter extends RecyclerView.Adapter<SprintsAdapter.ViewHolder> {
    private ArrayList<Sprint> sprints;
    private Context mCtx;

    public SprintsAdapter(ArrayList<Sprint> sprints, Context mCtx) {
        this.sprints = sprints;
        this.mCtx = mCtx;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sprint, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Sprint sprint = sprints.get(position);

        holder.codigo.setText(sprint.getCodigoSprint());
        holder.nombre.setText(sprint.getNombreSprint());
        holder.fecha_inicio.setText(Long.toString(sprint.getFechaCreacionSprint()));
        holder.fecha_fin.setText(Long.toString(sprint.getFechaFinSprint()));
        holder.estado.setText(sprint.getEstadoSprint());
        holder.list_sprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(mCtx, SprintAntiguoDetalle.class);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                myIntent.putExtra("sprint",sprint);
                //myIntent.putExtra("proyecto", proyecto);
                //myIntent.putExtra("usuario", usuario);
                mCtx.startActivity(myIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (sprints != null) {
            return sprints.size();
        } else {
            return 0;
        }
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        public final View view;
        public final TextView codigo;
        public final TextView nombre;
        public final TextView fecha_inicio;
        public final TextView fecha_fin;
        public final TextView estado;
        public final RelativeLayout list_sprint;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            codigo = view.findViewById(R.id.list_codigo_sprint);
            nombre = view.findViewById(R.id.list_nombre_sprint);
            fecha_inicio = view.findViewById(R.id.list_fecha_inicio_sprint);
            fecha_fin = view.findViewById(R.id.list_fecha_fin_sprint);
            estado = view.findViewById(R.id.list_estado_sprint);
            list_sprint = view.findViewById(R.id.list_sprint);
        }
    }
}