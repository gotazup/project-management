package com.example.usuario.gestionproyectos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsuariosAdapter extends RecyclerView.Adapter<UsuariosAdapter.ViewHolder> {
    private ArrayList<Usuario> users;
    private Context mCtx;
    RestServices restServices;

    public UsuariosAdapter(ArrayList<Usuario> users, Context mCtx) {
        this.users = users;
        this.mCtx = mCtx;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_usuarios, parent, false);
        restServices = ApiUtils.getRestService();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Usuario usuario = users.get(position);

        holder.nombre.setText(usuario.getNombreUsuario());
        holder.rol.setText(usuario.getCorreoUsuario());

        holder.modificar_eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(mCtx, holder.modificar_eliminar);
                popup.inflate(R.menu.menu_modificar_eliminar);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.modificar:
                                Intent myIntent = new Intent(mCtx, UsuarioUpd.class);
                                myIntent.putExtra("usuario",usuario);
                                mCtx.startActivity(myIntent);
                                return true;
                            case R.id.eliminar:
                                AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                                builder.setCancelable(true);
                                builder.setTitle("¿Está seguro?");
                                builder.setMessage("Se eliminará el usuario.");
                                builder.setPositiveButton("Aceptar",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                Usuario theRemovedItem = users.get(position);
                                                // remove your item from data base
                                                eliminarUsuario(usuario.getIdUsuario());
                                                users.remove(position);  // remove the item from list
                                                notifyItemRemoved(position); // notify the adapter about the removed item
                                            }
                                        });
                                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });

                                AlertDialog dialog = builder.create();
                                dialog.show();
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (users != null) {
            return users.size();
        } else {
            return 0;
        }
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        public final View view;
        public final TextView nombre;
        public final TextView rol;
        public final ImageView foto;
        public final ImageButton modificar_eliminar;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            nombre = view.findViewById(R.id.nombre_usuario);
            rol = view.findViewById(R.id.rol_usuario);
            foto = view.findViewById(R.id.foto_usuario);
            modificar_eliminar = view.findViewById(R.id.modificar_eliminar_usuario);
        }
    }

    public void eliminarUsuario(int id){
        Call<Usuario> call = restServices.deleteUser(id);
        call.enqueue(new Callback<Usuario>(){
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful()){
                    Toast.makeText(mCtx, "Usuario eliminado!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(mCtx,t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}