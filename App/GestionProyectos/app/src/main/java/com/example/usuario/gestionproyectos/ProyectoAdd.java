package com.example.usuario.gestionproyectos;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProyectoAdd extends AppCompatActivity {

    private LinearLayout parentLinearLayout;
    TextView codigo, nombre, fecha_inicio, duracion_sprint;
    RestServices restServices;
    ArrayList<Usuario> listUsuarios = new ArrayList<Usuario>();
    ArrayList<String> userNames = new ArrayList<String>();
    ArrayList<Rol> listRoles = new ArrayList<Rol>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyecto_add);
        restServices = ApiUtils.getRestService();

        getRoles();
        cargarUsuarios(getUsuarios());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_guardar:
                guardar();
                //Toast.makeText(this, "GUARDAAAA", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void guardar(){
        codigo = (TextView) findViewById(R.id.add_codigo_proyecto);
        nombre = (TextView) findViewById(R.id.add_nombre_proyecto);
        fecha_inicio = (TextView) findViewById(R.id.add_fecha_inicio_proyecto);
        duracion_sprint = (TextView) findViewById(R.id.add_duración_sprint_proyecto);
        long startDate=0;
        Timestamp ts=null;
        try {
            String dateString = fecha_inicio.getText().toString();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(dateString);

            startDate = date.getTime()/1000;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Proyecto proyecto = new Proyecto(codigo.getText().toString(), nombre.getText().toString(), startDate, Integer.parseInt(duracion_sprint.getText().toString()));

        ArrayList<TipoUs> listTipos = new ArrayList<TipoUs>();

        LinearLayout layout = findViewById(R.id.add_tipos);
        Integer count = layout.getChildCount();
        View v = null;
        for(int i=0; i<(count-1); i++) {
            v = layout.getChildAt(i);
            LinearLayout linear = (LinearLayout) v;
            View tv = linear.getChildAt(1);
            TextInputLayout textILayout = (TextInputLayout) tv;
            View til = textILayout.getChildAt(0);
            TextInputEditText textEdit = (TextInputEditText) ((FrameLayout) til).getChildAt(0);

            listTipos.add(new TipoUs(textEdit.getText().toString()));
        }

        proyecto.setTipoUsCollection(listTipos);

        ArrayList<UsuarioProyecto> listUsuariosProyecto = new ArrayList<UsuarioProyecto>();
        Rol rol;

        LinearLayout layout1 = findViewById(R.id.add_usuarios);
        Integer count1 = layout1.getChildCount();
        View v1 = null;
        for(int i=0; i<(count1-1); i++) {
            v1 = layout1.getChildAt(i);
            LinearLayout linear1 = (LinearLayout) v1;
            View iv = linear1.getChildAt(0);
            ImageView imageView = (ImageView) iv;
            View tv1 = linear1.getChildAt(1);
            TextInputLayout textILayout1 = (TextInputLayout) tv1;
            View til1 = textILayout1.getChildAt(0);
            MaterialBetterSpinner betterSpinner = (MaterialBetterSpinner) ((FrameLayout) til1).getChildAt(0);
            Usuario usuario = getUsuario(betterSpinner.getText().toString());

            //Toast.makeText(this, usuario.getIdUsuario().toString(), Toast.LENGTH_SHORT).show();

            if(imageView.getTag()=="red"){
                rol = getRol(1);
            }else{
                rol = getRol(2);
            }

            //UsuarioProyectoPK up = new UsuarioProyectoPK(3,usuario.getIdUsuario());
            listUsuariosProyecto.add(new UsuarioProyecto(usuario, rol));
        }
        proyecto.setIdEstadoProyecto(new EstadoProyecto(1,"AC","Activo"));
        proyecto.setUsuarioProyectoCollection(listUsuariosProyecto);

        //Toast.makeText(this, proyecto.getFechaInicioProyecto().toString(), Toast.LENGTH_SHORT).show();
        guardarProyecto(proyecto);

    }

    public void guardarProyecto(Proyecto proyecto){
        restServices = ApiUtils.getRestService();
        Call<Proyecto> call = restServices.addProyecto(proyecto);
        call.enqueue(new Callback<Proyecto>(){
            @Override
            public void onResponse(Call<Proyecto> call, Response<Proyecto> response) {
                if(response.isSuccessful()){
                    Toast.makeText(ProyectoAdd.this, "Proyecto creado correctamente!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Proyecto> call, Throwable t) {
                Toast.makeText(ProyectoAdd.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public List<String> getUsuarios(){
        Call<ArrayList<Usuario>> call = restServices.getUsuarios();
        call.enqueue(new Callback<ArrayList<Usuario>>(){
            @Override
            public void onResponse(Call<ArrayList<Usuario>> call, Response<ArrayList<Usuario>> response) {
                if(response.isSuccessful()){
                    listUsuarios = response.body();

                    for(int i=0;i<listUsuarios.size();i++){
                        userNames.add(listUsuarios.get(i).getNombreUsuario());
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Usuario>> call, Throwable t) {
                Toast.makeText(ProyectoAdd.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return userNames;
    }

    public void getRoles(){
        Call<ArrayList<Rol>> call = restServices.getRoles();
        call.enqueue(new Callback<ArrayList<Rol>>(){
            @Override
            public void onResponse(Call<ArrayList<Rol>> call, Response<ArrayList<Rol>> response) {
                if(response.isSuccessful()){
                    listRoles = response.body();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Rol>> call, Throwable t) {
                Toast.makeText(ProyectoAdd.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void cargarUsuarios(List<String> userNames){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ProyectoAdd.this,
                android.R.layout.simple_dropdown_item_1line, userNames);
        MaterialBetterSpinner betterSpinner = findViewById(R.id.add_usuario_proyecto);
        betterSpinner.setAdapter(adapter);
    }

    public void onAddTipo(View v) {
        parentLinearLayout = (LinearLayout) findViewById(R.id.add_tipos);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.add_tipo_tarea_proyecto, null);
        parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
    }

    public void onAddUsuario(View v) {
        parentLinearLayout = (LinearLayout) findViewById(R.id.add_usuarios);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.add_usuario_proyecto, null);
        parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, userNames);

        LinearLayout linear = (LinearLayout) rowView;
        TextInputLayout textILayout = (TextInputLayout) linear.getChildAt(1);
        View til = textILayout.getChildAt(0);

        MaterialBetterSpinner betterSpinner = (MaterialBetterSpinner) ((FrameLayout) til).getChildAt(0);
        betterSpinner.setAdapter(adapter1);
    }

    public void onDelete(View v) {
        parentLinearLayout.removeView((View) v.getParent());
    }

    public void onImportarTipos(View v){
        final AlertDialog diag = new AlertDialog.Builder(this)
                .setTitle("Importar tipos desde:")
                .setCancelable(true)
                .setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .create();

        View view = getLayoutInflater().inflate(R.layout.fragment_dialog_importar, null);

        ListView lv = (ListView) view.findViewById(R.id.importar_tipos);
        DialogImportarAdapter clad = new DialogImportarAdapter(initProyecto(), this);

        lv.setAdapter(clad);

        //lv.setOnItemClickListener(........);

        diag.setView(view);

        diag.show();


    }

    public ArrayList<Proyecto> initProyecto(){
        ArrayList<Proyecto> projects = null;

        return projects;
    }

    public void setLider(final View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Líder de proyecto");
        builder.setMessage("Seleccionar a este usuario como líder de proyecto.");
        builder.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ImageView imageV = (ImageView) v;

                        LinearLayout layout = findViewById(R.id.add_usuarios);
                        Integer count = layout.getChildCount();
                        View vi = null;
                        for(int i=0; i<(count-1); i++) {
                            vi = layout.getChildAt(i);
                            LinearLayout linear = (LinearLayout) vi;
                            View iv = linear.getChildAt(0);
                            ImageView imageView = (ImageView) iv;
                            imageView.setImageResource(R.drawable.ic_person);
                            imageView.setTag("gray");
                        }

                        imageV.setImageResource(R.drawable.ic_person_add);
                        imageV.setTag("red");
                    }
                });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public Usuario getUsuario(String nombre){
        Usuario user_obj = null;
        for (int i=0;i<listUsuarios.size();i++){
            if(listUsuarios.get(i).getNombreUsuario().equals(nombre)){
                user_obj = listUsuarios.get(i);
            }
        }
        return user_obj;
    }

    public Rol getRol(int id){
        Rol rol_obj = null;
        for(int i=0;i<listRoles.size();i++){
            if(listRoles.get(i).getIdRol()==id){
                rol_obj = listRoles.get(i);
            }
        }
        return rol_obj;
    }
}
