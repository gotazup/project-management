package com.example.usuario.gestionproyectos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class BacklogAdapter extends RecyclerView.Adapter<BacklogAdapter.ViewHolder> {
    private ArrayList<Tarea> tareas;
    private Context mCtx;

    public BacklogAdapter(ArrayList<Tarea> tareas, Context mCtx) {
        this.tareas = tareas;
        this.mCtx = mCtx;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_backlog, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Tarea tarea = tareas.get(position);
        String prioridadIcon="";

        holder.codigo.setText(tarea.getCodigo());
        holder.nombre.setText(tarea.getNombre());
        holder.tipo.setText(tarea.getIdTipoUs());
        holder.descripcion.setText(tarea.getDescripcion());
        String tipoPrioridad =  tarea.getPrioridad();
        switch (tipoPrioridad) {
            case "A":
                holder.prioridad.setImageResource(R.color.colorPrioridadAlta);
                break;
            case "M":
                holder.prioridad.setImageResource(R.color.colorPrioridadMedia);
                break;
            case "B":
                holder.prioridad.setImageResource(R.color.colorPrioridadBaja);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (tareas != null) {
            return tareas.size();
        } else {
            return 0;
        }
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        public final View view;
        public final TextView codigo;
        public final TextView nombre;
        public final TextView tipo;
        public final TextView descripcion;
        public final ImageView prioridad;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            codigo = view.findViewById(R.id.list_codigo_tarea_backlog);
            nombre = view.findViewById(R.id.list_nombre_tarea_backlog);
            tipo = view.findViewById(R.id.list_tipo_tarea_backlog);
            descripcion = view.findViewById(R.id.list_descripcion_tarea_backlog);
            prioridad = view.findViewById(R.id.list_prioridad_tarea_backlog);
        }
    }
}