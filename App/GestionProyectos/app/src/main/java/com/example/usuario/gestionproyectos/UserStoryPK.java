package com.example.usuario.gestionproyectos;

import java.io.Serializable;

public class UserStoryPK implements Serializable {


    private int idUserStory;
    private int idProyecto;

    public UserStoryPK() {
    }

    public UserStoryPK(int idUserStory, int idProyecto) {
        this.idUserStory = idUserStory;
        this.idProyecto = idProyecto;
    }

    public int getIdUserStory() {
        return idUserStory;
    }

    public void setIdUserStory(int idUserStory) {
        this.idUserStory = idUserStory;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUserStory;
        hash += (int) idProyecto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserStoryPK)) {
            return false;
        }
        UserStoryPK other = (UserStoryPK) object;
        if (this.idUserStory != other.idUserStory) {
            return false;
        }
        if (this.idProyecto != other.idProyecto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.UserStoryPK[ idUserStory=" + idUserStory + ", idProyecto=" + idProyecto + " ]";
    }

}
