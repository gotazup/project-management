package com.example.usuario.gestionproyectos;

import java.io.Serializable;

/**
 *
 * @author gustavo
 */
public class UsuarioProyecto implements Serializable {

    private static final long serialVersionUID = 1L;

    protected UsuarioProyectoPK usuarioProyectoPK;

    //private Proyecto proyecto1;

    private Rol idRol;

    private Usuario usuario;

    public UsuarioProyecto() {
    }

    public UsuarioProyecto(Usuario usuario, Rol rol) {
        this.usuario = usuario;
        this.idRol = rol;
    }

    public UsuarioProyecto(UsuarioProyectoPK usuarioProyectoPK) {
        this.usuarioProyectoPK = usuarioProyectoPK;
    }


    public UsuarioProyecto(int idProyecto, int idUsuario) {
        this.usuarioProyectoPK = new UsuarioProyectoPK(idProyecto, idUsuario);
    }

    public UsuarioProyectoPK getUsuarioProyectoPK() {
        return usuarioProyectoPK;
    }

    public void setUsuarioProyectoPK(UsuarioProyectoPK usuarioProyectoPK) {
        this.usuarioProyectoPK = usuarioProyectoPK;
    }

    /*public Proyecto getProyecto1() {
        return proyecto1;
    }

    public void setProyecto1(Proyecto proyecto1) {
        this.proyecto1 = proyecto1;
    }*/

    public Rol getIdRol() {
        return idRol;
    }

    public void setIdRol(Rol idRol) {
        this.idRol = idRol;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioProyectoPK != null ? usuarioProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioProyecto)) {
            return false;
        }
        UsuarioProyecto other = (UsuarioProyecto) object;
        if ((this.usuarioProyectoPK == null && other.usuarioProyectoPK != null) || (this.usuarioProyectoPK != null && !this.usuarioProyectoPK.equals(other.usuarioProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.UsuarioProyecto[ usuarioProyectoPK=" + usuarioProyectoPK + " ]";
    }

}
