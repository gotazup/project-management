package com.example.usuario.gestionproyectos;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsuarioList extends Base {


    RestServices restServices;
    ArrayList<Usuario> list = new ArrayList<Usuario>();
    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("usuario");

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_usuarios, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(4).setChecked(true);
        navigationView.getMenu().getItem(1).setVisible(false);
        navigationView.getMenu().getItem(2).setVisible(false);
        navigationView.getMenu().getItem(3).setVisible(false);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add_usuario);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addIntent = new Intent(getApplicationContext(), UsuarioAdd.class);
                addIntent.putExtra("usuario", usuario);
                startActivity(addIntent);
            }
        });

        restServices = ApiUtils.getRestService();
        initUsuario();
    }

    public void initUsuario(){
        Call<ArrayList<Usuario>> call = restServices.getUsuarios();
        call.enqueue(new Callback<ArrayList<Usuario>>(){
            @Override
            public void onResponse(Call<ArrayList<Usuario>> call, Response<ArrayList<Usuario>> response) {
                RecyclerView users;
                RecyclerView.Adapter adapter;
                if(response.isSuccessful()){
                    list = response.body();
                    users = (RecyclerView) findViewById(R.id.usuarios);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(UsuarioList.this);
                    users.setLayoutManager(mLayoutManager);

                    adapter = new UsuariosAdapter(list, UsuarioList.this);
                    users.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Usuario>> call, Throwable t) {
                Toast.makeText(UsuarioList.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle menu_modificar_eliminar view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_proyecto:
                Intent proyectoIntent = new Intent(getApplicationContext(), ProyectoList.class);
                proyectoIntent.putExtra("usuario", usuario);
                startActivity(proyectoIntent);
                return true;
            case R.id.nav_mis_tareas:
                Intent misTareasIntent = new Intent(getApplicationContext(), MisTareas.class);
                startActivity(misTareasIntent);
                return true;
            case R.id.nav_backlog:
                Intent backlogIntent = new Intent(getApplicationContext(), Backlog.class);
                startActivity(backlogIntent);
                return true;
            case R.id.nav_sprints:
                Intent sprintsIntent = new Intent(getApplicationContext(), SprintList.class);
                startActivity(sprintsIntent);
                return true;
            case R.id.nav_usuario:
                Intent usuariosIntent = new Intent(getApplicationContext(), UsuarioList.class);
                usuariosIntent.putExtra("usuario", usuario);
                startActivity(usuariosIntent);
                return true;
            default:
                return false;
        }
    }
}
