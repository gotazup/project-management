package com.example.usuario.restclient;

public class ApiUtils {

    private ApiUtils(){
    }

    public static final String BASE_URL = "http://10.0.2.2:8080/proyectos_agiles/webresources/";

    public static RestServices getRestService(){
        return RetrofitClient.getClient(BASE_URL).create(RestServices.class);
    }
}
