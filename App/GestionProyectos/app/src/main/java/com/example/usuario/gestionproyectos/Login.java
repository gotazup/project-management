package com.example.usuario.gestionproyectos;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {


    // UI references.
    private TextView mEmailView;
    private TextView mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private RestServices restService;
    private Button mEmailSignInButton;
    private Usuario usuario = new Usuario();
    private GoogleApiClient googleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        // Set up the login form.
        mEmailView =  findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        restService = ApiUtils.getRestService();

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = mEmailView.getText().toString();
                String password = mPasswordView.getText().toString();
                usuario.setNombreUsuario(email);
                usuario.setContrasenaUsuario(password);
                if(validarLogin(email, password)){
                    login(usuario);
                }

            }
        });


    }


    private boolean validarLogin(String username, String password){
        if(username == null || username.trim().length() == 0){
            Toast.makeText(this, "El usuario es requerido.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(password == null || password.trim().length() == 0){
            Toast.makeText(this, "El password es requerido", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void login(Usuario user){
        Call<Usuario> call = restService.login(user);
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful()){
                    Usuario usuarioRest = (Usuario) response.body();
                    if(usuarioRest != null){
                        Intent intent = new Intent(Login.this, ProyectoList.class);
                        intent.putExtra("usuario", usuarioRest);
                        startActivity(intent);
                    } else{
                        Toast.makeText(Login.this, "Credenciales invalidas", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Login.this, "Error! Intente de nuevo!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(Login.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

