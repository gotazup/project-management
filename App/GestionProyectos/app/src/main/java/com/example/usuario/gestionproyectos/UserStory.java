package com.example.usuario.gestionproyectos;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public class UserStory implements Serializable {

    private static final long serialVersionUID = 1L;

    protected UserStoryPK userStoryPK;

    private String codigoUserStory;

    private String descripcionUserStory;

    private int prioridadUserStory;

    private int tiempoPlanificadoUserStory;

    private Integer tiempoEjecutadoUserStory;

    private long fechaCreacionUserStory;

    private long fechaFinUserStory;

    private String nombreUs;

    private EstadoUs idEstadoUs;

    private Proyecto proyecto;

    private Sprint sprint;

    private TipoUs idTipoUs;

    private Usuario idUsuarioCreacionUserStory;

    private Usuario idUsuarioAsignadoUserStory;

    private Collection<NotaUs> notaUsCollection;

    public UserStory() {
    }

    public UserStory(UserStoryPK userStoryPK) {
        this.userStoryPK = userStoryPK;
    }

    public UserStory(String codigoUserStory, String nombreUs, String descripcionUserStory, int prioridadUserStory, int tiempoPlanificadoUserStory, int tiempoEjecutadoUserStory, long fechaCreacionUserStory, long fechaFin, TipoUs tipoUs, Usuario idUsuarioAsignadoUserStory) {
        this.userStoryPK = userStoryPK;
        this.codigoUserStory = codigoUserStory;
        this.nombreUs = nombreUs;
        this.descripcionUserStory = descripcionUserStory;
        this.prioridadUserStory = prioridadUserStory;
        this.tiempoPlanificadoUserStory = tiempoPlanificadoUserStory;
        this.tiempoEjecutadoUserStory = tiempoEjecutadoUserStory;
        this.fechaCreacionUserStory = fechaCreacionUserStory;
        this.fechaFinUserStory = fechaFin;
        this.idTipoUs = tipoUs;
        this.idUsuarioAsignadoUserStory = idUsuarioAsignadoUserStory;

    }

    public UserStory(String codigo, String nombre, TipoUs idTipoUs, String descripcion, int prioridad) {
        this.codigoUserStory = codigo;
        this.nombreUs = nombre;
        this.idTipoUs = idTipoUs;
        this.descripcionUserStory = descripcion;
        this.prioridadUserStory = prioridad;
    }

    public UserStory(int idUserStory, int idProyecto) {
        this.userStoryPK = new UserStoryPK(idUserStory, idProyecto);
    }

    public UserStoryPK getUserStoryPK() {
        return userStoryPK;
    }

    public void setUserStoryPK(UserStoryPK userStoryPK) {
        this.userStoryPK = userStoryPK;
    }

    public String getCodigoUserStory() {
        return codigoUserStory;
    }

    public void setCodigoUserStory(String codigoUserStory) {
        this.codigoUserStory = codigoUserStory;
    }

    public String getDescripcionUserStory() {
        return descripcionUserStory;
    }

    public void setDescripcionUserStory(String descripcionUserStory) {
        this.descripcionUserStory = descripcionUserStory;
    }

    public int getPrioridadUserStory() {
        return prioridadUserStory;
    }

    public void setPrioridadUserStory(int prioridadUserStory) {
        this.prioridadUserStory = prioridadUserStory;
    }

    public int getTiempoPlanificadoUserStory() {
        return tiempoPlanificadoUserStory;
    }

    public void setTiempoPlanificadoUserStory(int tiempoPlanificadoUserStory) {
        this.tiempoPlanificadoUserStory = tiempoPlanificadoUserStory;
    }

    public Integer getTiempoEjecutadoUserStory() {
        return tiempoEjecutadoUserStory;
    }

    public void setTiempoEjecutadoUserStory(Integer tiempoEjecutadoUserStory) {
        this.tiempoEjecutadoUserStory = tiempoEjecutadoUserStory;
    }

    public long getFechaCreacionUserStory() {
        return fechaCreacionUserStory;
    }

    public void setFechaCreacionUserStory(long fechaCreacionUserStory) {
        this.fechaCreacionUserStory = fechaCreacionUserStory;
    }

    public long getFechaFinUserStory() {
        return fechaFinUserStory;
    }

    public void setFechaFinUserStory(long fechaFinUserStory) {
        this.fechaFinUserStory = fechaFinUserStory;
    }

    public String getNombreUs() {
        return nombreUs;
    }

    public void setNombreUs(String nombreUs) {
        this.nombreUs = nombreUs;
    }

    public EstadoUs getIdEstadoUs() {
        return idEstadoUs;
    }

    public void setIdEstadoUs(EstadoUs idEstadoUs) {
        this.idEstadoUs = idEstadoUs;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public Sprint getSprint() {
        return sprint;
    }

    public void setSprint(Sprint sprint) {
        this.sprint = sprint;
    }

    public TipoUs getIdTipoUs() {
        return idTipoUs;
    }

    public void setIdTipoUs(TipoUs idTipoUs) {
        this.idTipoUs = idTipoUs;
    }

    public Usuario getIdUsuarioCreacionUserStory() {
        return idUsuarioCreacionUserStory;
    }

    public void setIdUsuarioCreacionUserStory(Usuario idUsuarioCreacionUserStory) {
        this.idUsuarioCreacionUserStory = idUsuarioCreacionUserStory;
    }

    public Usuario getIdUsuarioAsignadoUserStory() {
        return idUsuarioAsignadoUserStory;
    }

    public void setIdUsuarioAsignadoUserStory(Usuario idUsuarioAsignadoUserStory) {
        this.idUsuarioAsignadoUserStory = idUsuarioAsignadoUserStory;
    }

    public Collection<NotaUs> getNotaUsCollection() {
        return notaUsCollection;
    }

    public void setNotaUsCollection(Collection<NotaUs> notaUsCollection) {
        this.notaUsCollection = notaUsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userStoryPK != null ? userStoryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserStory)) {
            return false;
        }
        UserStory other = (UserStory) object;
        if ((this.userStoryPK == null && other.userStoryPK != null) || (this.userStoryPK != null && !this.userStoryPK.equals(other.userStoryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.UserStory[ userStoryPK=" + userStoryPK + " ]";
    }

}
