package com.example.usuario.gestionproyectos;

import java.util.Date;

public class Tarea {
    private static final long serialVersionUID = 1L;
    private Integer idUserStory;
    private String codigo;
    private String nombre;
    private String descripcion;
    private int valorNegocio;
    private int valorTecnico;
    private String prioridad;
    private int tiempoPlanificado;
    private Float tiempoEjecutado;
    private Date fechaCreacion;
    private Date fechaFin;
    private String idEstado;
    private String idProyecto;
    private String  idSprint;
    private String idTipoUs;
    private String idUsuario;

    public Tarea() {
        /*
         * Constructor de la clase (no recibe parametros).
         */
    }

    public Tarea(Integer idUserStory) {
        /*
         * Constructor de la clase.
         * @param id del User Story
         */
        this.idUserStory = idUserStory;
    }

    public Tarea(String codigo, String nombre, String idTipoUs, String descripcion, String prioridad) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.idTipoUs = idTipoUs;
        this.descripcion = descripcion;
        this.prioridad = prioridad;
    }

    public Integer getIdUserStory() {
        /*
         * Retorna el id del User Story.
         */
        return idUserStory;
    }

    public void setIdUserStory(Integer idUserStory) {
        this.idUserStory = idUserStory;
    }

    public String getCodigo() {
        /*
         * Retorna el codigo del User Story.
         */
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        /*
         * Retorna el codigo del User Story.
         */
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        /*
         * Retorna la descripcion del User Story.
         */
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getValorNegocio() {
        /*
         * Retorna el valor de negocio del User Story.
         */
        return valorNegocio;
    }

    public void setValorNegocio(int valorNegocio) {
        this.valorNegocio = valorNegocio;
    }

    public int getValorTecnico() {
        /*
         * Retorna el valor tecnico del User Story.
         */
        return valorTecnico;
    }

    public void setValorTecnico(int valorTecnico) {
        this.valorTecnico = valorTecnico;
    }

    public String getPrioridad() {
        /*
         * Retorna la prioridad del User Story.
         */
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public int getTiempoPlanificado() {
        /*
         * Retorna el tiempo planificado del User Story.
         */
        return tiempoPlanificado;
    }

    public void setTiempoPlanificado(int tiempoPlanificado) {
        this.tiempoPlanificado = tiempoPlanificado;
    }

    public Float getTiempoEjecutado() {
        /*
         * Retorna el tiempo ejecutado del User Story.
         */
        return tiempoEjecutado;
    }

    public void setTiempoEjecutado(Float tiempoEjecutado) {
        this.tiempoEjecutado = tiempoEjecutado;
    }

    public Date getFechaCreacion() {
        /*
         * Retorna la fecha de creacion del User Story.
         */
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(String idEstado) {
        this.idEstado = idEstado;
    }

    public String getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(String idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(String idSprint) {
        this.idSprint = idSprint;
    }

    public String getIdTipoUs() {
        return idTipoUs;
    }

    public void setIdTipoUs(String idTipoUs) {
        this.idTipoUs = idTipoUs;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }


}
