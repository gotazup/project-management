package com.example.usuario.gestionproyectos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MisTareasTerminadas extends Fragment {

    private RecyclerView tareas_terminadas;
    private RecyclerView.Adapter adapter;
    private Usuario usuario;

    public void MisTareasTerminadas(Usuario usuario){
        this.usuario = usuario;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mis_tareas_terminadas, container, false);

        Bundle args = getArguments();
        usuario = (Usuario) args.getSerializable("usuario");

        ArrayList<UserStory> tareas_terminadas = initMisTareasTerminadas();
        this.tareas_terminadas =(RecyclerView) view.findViewById(R.id.tareas_terminadas);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext().getApplicationContext());
        this.tareas_terminadas.setLayoutManager(mLayoutManager);

        adapter = new MisTareasAdapter(tareas_terminadas,getContext().getApplicationContext(), usuario);
        this.tareas_terminadas.setAdapter(adapter);

        return view;
    }

    private ArrayList<UserStory> initMisTareasTerminadas(){
        ArrayList<UserStory> list = new ArrayList<>();

        list.add(new UserStory("PR1-6", "Agregar botón volver", new TipoUs("Desarrollo"),"Botón volver en vista creación de tareas",2));

        return list;
    }
}
