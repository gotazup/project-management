package com.example.usuario.gestionproyectos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

public class Proyecto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idProyecto;

    private String codigoProyecto;

    private String nombreProyecto;

    private long fechaInicioProyecto;

    private long fechaFinProyecto;

    private int duracionSprintsProyecto;

    private Collection<TipoUs> tipoUsCollection;

    private Collection<UserStory> userStoryCollection;

    private Collection<UsuarioProyecto> usuarioProyectoCollection;

    private Collection<Sprint> sprintCollection;

    private EstadoProyecto idEstadoProyecto;

    public Proyecto() {
    }

    public Proyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public Proyecto(String codigoProyecto, String nombreProyecto, long fechaInicioProyecto, int duracionSprintsProyecto) {
        this.codigoProyecto = codigoProyecto;
        this.nombreProyecto = nombreProyecto;
        this.fechaInicioProyecto = fechaInicioProyecto;
        this.duracionSprintsProyecto = duracionSprintsProyecto;
    }


    public Integer getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(Integer idProyecto) {
        this.idProyecto = idProyecto;
    }

    public String getCodigoProyecto() {
        return codigoProyecto;
    }

    public void setCodigoProyecto(String codigoProyecto) {
        this.codigoProyecto = codigoProyecto;
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public long getFechaInicioProyecto() {
        return fechaInicioProyecto;
    }

    public void setFechaInicioProyecto(long fechaInicioProyecto) {
        this.fechaInicioProyecto = fechaInicioProyecto;
    }

    public long getFechaFinProyecto() {
        return fechaFinProyecto;
    }

    public void setFechaFinProyecto(long fechaFinProyecto) {
        this.fechaFinProyecto = fechaFinProyecto;
    }

    public int getDuracionSprintsProyecto() {
        return duracionSprintsProyecto;
    }

    public void setDuracionSprintsProyecto(int duracionSprintsProyecto) {
        this.duracionSprintsProyecto = duracionSprintsProyecto;
    }

    public Collection<TipoUs> getTipoUsCollection() {
        return tipoUsCollection;
    }

    public void setTipoUsCollection(Collection<TipoUs> tipoUsCollection) {
        this.tipoUsCollection = tipoUsCollection;
    }

    public Collection<UserStory> getUserStoryCollection() {
        return userStoryCollection;
    }

    public void setUserStoryCollection(Collection<UserStory> userStoryCollection) {
        this.userStoryCollection = userStoryCollection;
    }

    public Collection<UsuarioProyecto> getUsuarioProyectoCollection() {
        return usuarioProyectoCollection;
    }

    public void setUsuarioProyectoCollection(Collection<UsuarioProyecto> usuarioProyectoCollection) {
        this.usuarioProyectoCollection = usuarioProyectoCollection;
    }

    public Collection<Sprint> getSprintCollection() {
        return sprintCollection;
    }

    public void setSprintCollection(Collection<Sprint> sprintCollection) {
        this.sprintCollection = sprintCollection;
    }

    public EstadoProyecto getIdEstadoProyecto() {
        return idEstadoProyecto;
    }

    public void setIdEstadoProyecto(EstadoProyecto idEstadoProyecto) {
        this.idEstadoProyecto = idEstadoProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProyecto != null ? idProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.idProyecto == null && other.idProyecto != null) || (this.idProyecto != null && !this.idProyecto.equals(other.idProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.Proyecto[ idProyecto=" + idProyecto + " ]";
    }

}

