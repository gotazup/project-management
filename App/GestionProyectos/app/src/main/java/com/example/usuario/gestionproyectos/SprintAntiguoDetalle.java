package com.example.usuario.gestionproyectos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;
import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SprintAntiguoDetalle extends AppCompatActivity {

    private TextView codigo, nombre, fecha_inicio, fecha_fin, estado;
    RestServices restServices;
    ArrayList<UserStory> list = new ArrayList<UserStory>();
    Sprint sprint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sprint_antiguo);
        restServices = ApiUtils.getRestService();

        Bundle extras = getIntent().getExtras();
        sprint = (Sprint) extras.getSerializable("sprint");

        cargar(sprint);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_generar_reporte, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void cargar(Sprint s){
        codigo = (TextView) findViewById(R.id.det_codigo_sprint);
        codigo.setText(s.getCodigoSprint());
        nombre = (TextView) findViewById(R.id.det_nombre_sprint);
        nombre.setText(s.getNombreSprint());
        fecha_inicio = (TextView) findViewById(R.id.det_fecha_inicio_sprint);
        fecha_inicio.setText(Long.toString(s.getFechaCreacionSprint()));
        fecha_fin = (TextView) findViewById(R.id.det_fecha_fin_sprint);
        fecha_fin.setText(Long.toString(s.getFechaFinSprint()));
        estado = (TextView) findViewById(R.id.det_estado_sprint);
        estado.setText(s.getEstadoSprint());

        Call<ArrayList<UserStory>> call = restServices.getTareasPorSprint();
        call.enqueue(new Callback<ArrayList<UserStory>>(){
            @Override
            public void onResponse(Call<ArrayList<UserStory>> call, Response<ArrayList<UserStory>> response) {
                RecyclerView tareas;
                RecyclerView.Adapter adapter;
                if(response.isSuccessful()){
                    list = response.body();

                    tareas = (RecyclerView) findViewById(R.id.tareas_sprint_antiguo);
                    RecyclerView.LayoutManager mLayoutManagerrr = new LinearLayoutManager(SprintAntiguoDetalle.this);
                    tareas.setLayoutManager(mLayoutManagerrr);

                    adapter = new SprintAntiguoDetalleAdapter(list, SprintAntiguoDetalle.this);
                    tareas.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserStory>> call, Throwable t) {
                Toast.makeText(SprintAntiguoDetalle.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
