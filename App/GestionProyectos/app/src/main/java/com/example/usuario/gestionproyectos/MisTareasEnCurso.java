package com.example.usuario.gestionproyectos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class MisTareasEnCurso extends Fragment {

    private RecyclerView tareas_en_curso;
    private RecyclerView.Adapter adapter;
    private Usuario usuario;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mis_tareas_en_curso, container, false);

        Bundle args = getArguments();
        usuario = (Usuario) args.getSerializable("usuario");

        ArrayList<UserStory> tareas_en_curso = initMisTareasEnCurso();
        this.tareas_en_curso =(RecyclerView) view.findViewById(R.id.tareas_en_curso);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext().getApplicationContext());
        this.tareas_en_curso.setLayoutManager(mLayoutManager);

        adapter = new MisTareasAdapter(tareas_en_curso,getContext().getApplicationContext(),usuario);
        this.tareas_en_curso.setAdapter(adapter);

        return view;
    }

    private ArrayList<UserStory> initMisTareasEnCurso(){
        ArrayList<UserStory> list = new ArrayList<>();

        list.add(new UserStory("PR1-4", "Agregar botón guardar", new TipoUs("Desarrollo"),"Botón guardar en vista creación de tareas",1));
        list.add(new UserStory("PR1-5", "Probar funcionalidades del módulo UsuarioList", new TipoUs("Desarrollo"),"Probar ABM",2));

        return list;
    }
}
