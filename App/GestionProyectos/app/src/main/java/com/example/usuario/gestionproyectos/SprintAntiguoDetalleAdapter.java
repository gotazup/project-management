package com.example.usuario.gestionproyectos;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SprintAntiguoDetalleAdapter extends RecyclerView.Adapter<SprintAntiguoDetalleAdapter.ViewHolder> {
    private ArrayList<UserStory> tareas;
    private Context mCtx;

    public SprintAntiguoDetalleAdapter(ArrayList<UserStory> tareas, Context mCtx) {
        this.tareas = tareas;
        this.mCtx = mCtx;
    }


    @Override
    public SprintAntiguoDetalleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_sprint_antiguo, parent, false);

        return new SprintAntiguoDetalleAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SprintAntiguoDetalleAdapter.ViewHolder holder, final int position) {
        UserStory tarea = tareas.get(position);

        int tipoPrioridad =  tarea.getPrioridadUserStory();
        switch (tipoPrioridad) {
            case 1:
                holder.prioridad.setImageResource(R.color.colorPrioridadAlta);
                break;
            case 2:
                holder.prioridad.setImageResource(R.color.colorPrioridadMedia);
                break;
            case 3:
                holder.prioridad.setImageResource(R.color.colorPrioridadBaja);
                break;
        }
        holder.codigo.setText(tarea.getCodigoUserStory());
        holder.nombre.setText(tarea.getNombreUs());
        holder.tipo.setText(tarea.getIdTipoUs().getNombreTipoUs());
        holder.descripcion.setText(tarea.getDescripcionUserStory());

    }

    @Override
    public int getItemCount() {
        if (tareas != null) {
            return tareas.size();
        } else {
            return 0;
        }
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        public final View view;
        public final ImageView prioridad;
        public final TextView codigo;
        public final TextView nombre;
        public final TextView tipo;
        public final TextView descripcion;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            prioridad = view.findViewById(R.id.list_prioridad_tarea_sprint_antiguo);
            codigo = view.findViewById(R.id.list_codigo_tarea_sprint_antiguo);
            nombre = view.findViewById(R.id.list_nombre_tarea_sprint_antiguo);
            tipo = view.findViewById(R.id.list_tipo_tarea_sprint_antiguo);
            descripcion = view.findViewById(R.id.list_descripcion_tarea_sprint_antiguo);
        }
    }
}
