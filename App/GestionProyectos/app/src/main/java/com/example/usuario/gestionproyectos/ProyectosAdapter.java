package com.example.usuario.gestionproyectos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProyectosAdapter extends RecyclerView.Adapter<ProyectosAdapter.ViewHolder> {
    private ArrayList<Proyecto> projects;
    private Context mCtx;
    public Usuario usuario;
    RestServices restServices;

    public ProyectosAdapter(ArrayList<Proyecto> projects, Context mCtx, Usuario usuario) {
        this.projects = projects;
        this.mCtx = mCtx;
        this.usuario = usuario;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_proyectos, parent, false);

        restServices = ApiUtils.getRestService();

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Proyecto proyecto = projects.get(position);

        holder.codigo.setText(proyecto.getCodigoProyecto());
        holder.nombre.setText(proyecto.getNombreProyecto());

        holder.fecha_inicio.setText("Probando1");
        holder.fecha_fin.setText("Probando2");
        holder.estado.setText(proyecto.getIdEstadoProyecto().getDescripcionEstadoProyecto());

        if(usuario.getIsSuperUser()==1) {
            holder.modificar_eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupMenu popup = new PopupMenu(mCtx, holder.modificar_eliminar);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.menu_modificar_eliminar);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.modificar:
                                    Intent myIntent = new Intent(mCtx, ProyectoUpd.class);
                                    mCtx.startActivity(myIntent);
                                    return true;
                                case R.id.eliminar:
                                    AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                                    builder.setCancelable(true);
                                    builder.setTitle("¿Está seguro?");
                                    builder.setMessage("Se eliminará el proyecto y todas las tareas del mismo.");
                                    builder.setPositiveButton("Aceptar",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Proyecto theRemovedItem = projects.get(position);
                                                    // remove your item from data base
                                                    eliminarProyecto(proyecto.getIdProyecto());
                                                    projects.remove(position);  // remove the item from list
                                                    notifyItemRemoved(position); // notify the adapter about the removed item
                                                }
                                            });
                                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });

                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    //displaying the popup
                    popup.show();
                }
            });

        }else{
            holder.modificar_eliminar.setVisibility(View.INVISIBLE);
        }
        holder.cardProyecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(mCtx, MisTareas.class);
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                myIntent.putExtra("proyecto", proyecto);
                myIntent.putExtra("usuario", usuario);
                mCtx.startActivity(myIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (projects != null) {
            return projects.size();
        } else {
            return 0;
        }
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        public final View view;
        public final TextView codigo;
        public final TextView nombre;
        public final TextView fecha_inicio;
        public final TextView fecha_fin;
        public final TextView estado;
        public final ImageButton modificar_eliminar;
        public final CardView cardProyecto;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            codigo = view.findViewById(R.id.list_codigo_proyecto);
            nombre = view.findViewById(R.id.list_nombre_proyecto);
            fecha_inicio = view.findViewById(R.id.list_fecha_inicio_proyecto);
            fecha_fin = view.findViewById(R.id.list_fecha_fin_proyecto);
            estado = view.findViewById(R.id.list_estado_proyecto);
            modificar_eliminar = view.findViewById(R.id.mofificar_eliminar_proyecto);
            cardProyecto = view.findViewById(R.id.cardProyecto);
        }
    }

    public void eliminarProyecto(int id){
        Call<Proyecto> call = restServices.deleteProyecto(id);
        call.enqueue(new Callback<Proyecto>(){
            @Override
            public void onResponse(Call<Proyecto> call, Response<Proyecto> response) {
                if(response.isSuccessful()){
                    Toast.makeText(mCtx, "Proyecto eliminado!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Proyecto> call, Throwable t) {
                Toast.makeText(mCtx,t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}