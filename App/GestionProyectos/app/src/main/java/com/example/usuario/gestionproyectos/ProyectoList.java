package com.example.usuario.gestionproyectos;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProyectoList extends Base {


    private Usuario usuario;

    RestServices restServices;
    ArrayList<Proyecto> list = new ArrayList<Proyecto>();
    TextView nombre, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("usuario");

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_proyectos, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        nombre = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_nombre_usuario);
        nombre.setText(usuario.getNombreUsuario());
        email = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_email_usuario);
        email.setText(usuario.getCorreoUsuario());
        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.getMenu().getItem(1).setVisible(false);
        navigationView.getMenu().getItem(2).setVisible(false);
        navigationView.getMenu().getItem(3).setVisible(false);
        navigationView.getMenu().getItem(4).setVisible(false);

        if(usuario.getIsSuperUser() == 1){
            navigationView.getMenu().getItem(4).setVisible(true);
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add_proyecto);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(ProyectoList.this, ProyectoAdd.class));
                }
            });
        }

        restServices = ApiUtils.getRestService();
        initProyecto();
    }

    private void initProyecto(){
        Call<ArrayList<Proyecto>> call = restServices.getProyectos();
        call.enqueue(new Callback<ArrayList<Proyecto>>(){
            @Override
            public void onResponse(Call<ArrayList<Proyecto>> call, Response<ArrayList<Proyecto>> response) {
                RecyclerView projects;
                RecyclerView.Adapter adapter;

                if(response.isSuccessful()){
                    list = response.body();
                    projects = (RecyclerView) findViewById(R.id.proyectos);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProyectoList.this);
                    projects.setLayoutManager(mLayoutManager);

                    adapter = new ProyectosAdapter(list, ProyectoList.this, usuario);
                    projects.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Proyecto>> call, Throwable t) {
                Toast.makeText(ProyectoList.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle menu_modificar_eliminar view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_proyecto:
                Intent proyectoIntent = new Intent(getApplicationContext(), ProyectoList.class);
                proyectoIntent.putExtra("usuario", usuario);
                startActivity(proyectoIntent);
                return true;
            case R.id.nav_mis_tareas:
                Intent misTareasIntent = new Intent(getApplicationContext(), MisTareas.class);
                startActivity(misTareasIntent);
                return true;
            case R.id.nav_backlog:
                Intent backlogIntent = new Intent(getApplicationContext(), Backlog.class);
                startActivity(backlogIntent);
                return true;
            case R.id.nav_sprints:
                Intent sprintsIntent = new Intent(getApplicationContext(), SprintList.class);
                startActivity(sprintsIntent);
                return true;
            case R.id.nav_usuario:
                Intent usuariosIntent = new Intent(getApplicationContext(), UsuarioList.class);
                usuariosIntent.putExtra("usuario", usuario);
                startActivity(usuariosIntent);
                return true;
            default:
                return false;
        }
    }
}
