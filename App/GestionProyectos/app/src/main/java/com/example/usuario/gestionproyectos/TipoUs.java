package com.example.usuario.gestionproyectos;

import java.io.Serializable;
import java.util.Collection;

public class TipoUs implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer idTipoUs;

    private String nombreTipoUs;

    private Collection<Proyecto> proyectoCollection;

    private Collection<UserStory> userStoryCollection;

    public TipoUs() {
    }

    public TipoUs(Integer idTipoUs) {
        this.idTipoUs = idTipoUs;
    }

    public TipoUs(String nombreTipoUs) {
        this.nombreTipoUs = nombreTipoUs;
    }

    public Integer getIdTipoUs() {
        return idTipoUs;
    }

    public void setIdTipoUs(Integer idTipoUs) {
        this.idTipoUs = idTipoUs;
    }

    public String getNombreTipoUs() {
        return nombreTipoUs;
    }

    public void setNombreTipoUs(String nombreTipoUs) {
        this.nombreTipoUs = nombreTipoUs;
    }

    public Collection<Proyecto> getProyectoCollection() {
        return proyectoCollection;
    }

    public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
        this.proyectoCollection = proyectoCollection;
    }

    public Collection<UserStory> getUserStoryCollection() {
        return userStoryCollection;
    }

    public void setUserStoryCollection(Collection<UserStory> userStoryCollection) {
        this.userStoryCollection = userStoryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoUs != null ? idTipoUs.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoUs)) {
            return false;
        }
        TipoUs other = (TipoUs) object;
        if ((this.idTipoUs == null && other.idTipoUs != null) || (this.idTipoUs != null && !this.idTipoUs.equals(other.idTipoUs))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.TipoUs[ idTipoUs=" + idTipoUs + " ]";
    }

}
