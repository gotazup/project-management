package com.example.usuario.gestionproyectos;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;

import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SprintList extends Base {
    RestServices restServices;
    ArrayList<Sprint> list = new ArrayList<Sprint>();
    Sprint sprintActual = new Sprint();
    boolean actual=false;
    Usuario usuario;
    Proyecto proyecto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("usuario");
        proyecto = (Proyecto) extras.getSerializable("proyecto");

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_sprints, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(3).setChecked(true);

        restServices = ApiUtils.getRestService();
        initSprint();
    }

    private void initSprint(){
        Call<ArrayList<Sprint>> call = restServices.getSprints();
        call.enqueue(new Callback<ArrayList<Sprint>>(){
            @Override
            public void onResponse(Call<ArrayList<Sprint>> call, Response<ArrayList<Sprint>> response) {
                RecyclerView sprints;
                RecyclerView.Adapter adapter;
                if(response.isSuccessful()){
                    list = response.body();

                    for (int i = 0; i < list.size(); i++) {
                        if(list.get(i).getEstadoSprint()=="AC"){
                            sprintActual = list.get(i);
                            actual = true;
                            TextView codigo = (TextView) findViewById(R.id.list_codigo_sprint);
                            codigo.setText(sprintActual.getCodigoSprint());
                            TextView nombre = (TextView) findViewById(R.id.list_nombre_sprint);
                            nombre.setText(sprintActual.getNombreSprint());
                            TextView fecha_inicio = (TextView) findViewById(R.id.list_fecha_inicio_sprint);
                            fecha_inicio.setText(Long.toString(sprintActual.getFechaCreacionSprint()));
                            TextView fecha_fin = (TextView) findViewById(R.id.list_fecha_fin_sprint);
                            fecha_fin.setText(Long.toString(sprintActual.getFechaFinSprint()));
                        }
                    }

                    if(actual == false){
                        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add_sprint);
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(SprintList.this, SprintAdd.class));
                            }
                        });
                    }

                    sprints = (RecyclerView) findViewById(R.id.sprints_antiguos);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SprintList.this);
                    sprints.setLayoutManager(mLayoutManager);

                    adapter = new SprintsAdapter(list, SprintList.this);
                    sprints.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Sprint>> call, Throwable t) {
                Toast.makeText(SprintList.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle menu_modificar_eliminar view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_proyecto:
                Intent proyectoIntent = new Intent(getApplicationContext(), ProyectoList.class);
                proyectoIntent.putExtra("usuario", usuario);
                startActivity(proyectoIntent);
                return true;
            case R.id.nav_mis_tareas:
                Intent misTareasIntent = new Intent(getApplicationContext(), MisTareas.class);
                misTareasIntent.putExtra("proyecto", proyecto);
                misTareasIntent.putExtra("usuario", usuario);
                startActivity(misTareasIntent);
                return true;
            case R.id.nav_backlog:
                Intent backlogIntent = new Intent(getApplicationContext(), Backlog.class);
                backlogIntent.putExtra("proyecto", proyecto);
                backlogIntent.putExtra("usuario", usuario);
                startActivity(backlogIntent);
                return true;
            case R.id.nav_sprints:
                Intent sprintsIntent = new Intent(getApplicationContext(), SprintList.class);
                sprintsIntent.putExtra("proyecto", proyecto);
                sprintsIntent.putExtra("usuario", usuario);
                startActivity(sprintsIntent);
                return true;
            case R.id.nav_usuario:
                Intent usuariosIntent = new Intent(getApplicationContext(), UsuarioList.class);
                usuariosIntent.putExtra("usuario", usuario);
                startActivity(usuariosIntent);
                return true;
            default:
                return false;
        }
    }
}
