package com.example.usuario.gestionproyectos;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;

public class Backlog extends Base {

    private RecyclerView tareas_backlog;
    private RecyclerView.Adapter adapter;
    String[] roles = {"Administrador","Usuario Normal"};
    Usuario usuario;
    Proyecto proyecto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("usuario");
        proyecto = (Proyecto) extras.getSerializable("proyecto");

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_backlog, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(2).setChecked(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add_tarea);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Backlog.this, TareaAdd.class));
            }
        });

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, roles);
        MaterialBetterSpinner betterSpinner = findViewById(R.id.search_usuario);
        betterSpinner.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, roles);
        MaterialBetterSpinner betterSpinner2 = findViewById(R.id.search_tipo);
        betterSpinner2.setAdapter(adapter2);

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, roles);
        MaterialBetterSpinner betterSpinner3 = findViewById(R.id.search_prioridad);
        betterSpinner3.setAdapter(adapter3);

        ArrayList<Tarea> tareas = initBacklog();
        this.tareas_backlog = (RecyclerView) findViewById(R.id.backlog);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(Backlog.this);
        this.tareas_backlog.setLayoutManager(mLayoutManager);

        adapter = new BacklogAdapter(tareas, this);
        this.tareas_backlog.setAdapter(adapter);
    }

    private ArrayList<Tarea> initBacklog(){
        ArrayList<Tarea> list = new ArrayList<>();

        list.add(new Tarea("PR1-1", "Agregar botón guardar", "Desarrollo","Botón guardar en vista creación de tareas","A"));
        list.add(new Tarea("PR1-2", "Agregar botón volver", "Desarrollo","Botón volver en vista creación de tareas","B"));
        list.add(new Tarea("PR1-3", "Probar funcionalidades del módulo UsuarioList", "Testing","Probar ABM","M"));

        return list;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle menu_modificar_eliminar view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_proyecto:
                Intent proyectoIntent = new Intent(getApplicationContext(), ProyectoList.class);
                proyectoIntent.putExtra("usuario", usuario);
                startActivity(proyectoIntent);
                return true;
            case R.id.nav_mis_tareas:
                Intent misTareasIntent = new Intent(getApplicationContext(), MisTareas.class);
                misTareasIntent.putExtra("proyecto", proyecto);
                misTareasIntent.putExtra("usuario", usuario);
                startActivity(misTareasIntent);
                return true;
            case R.id.nav_backlog:
                Intent backlogIntent = new Intent(getApplicationContext(), Backlog.class);
                backlogIntent.putExtra("proyecto", proyecto);
                backlogIntent.putExtra("usuario", usuario);
                startActivity(backlogIntent);
                return true;
            case R.id.nav_sprints:
                Intent sprintsIntent = new Intent(getApplicationContext(), SprintList.class);
                sprintsIntent.putExtra("proyecto", proyecto);
                sprintsIntent.putExtra("usuario", usuario);
                startActivity(sprintsIntent);
                return true;
            case R.id.nav_usuario:
                Intent usuariosIntent = new Intent(getApplicationContext(), UsuarioList.class);
                usuariosIntent.putExtra("usuario", usuario);
                startActivity(usuariosIntent);
                return true;
            default:
                return false;
        }
    }
}
