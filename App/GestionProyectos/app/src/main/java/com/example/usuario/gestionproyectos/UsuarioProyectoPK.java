package com.example.usuario.gestionproyectos;

import java.io.Serializable;

public class UsuarioProyectoPK implements Serializable {
    private int idProyecto;
    private int idUsuario;

    public UsuarioProyectoPK() {
    }

    public UsuarioProyectoPK(int idProyecto, int idUsuario) {
        this.idProyecto = idProyecto;
        this.idUsuario = idUsuario;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idProyecto;
        hash += (int) idUsuario;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioProyectoPK)) {
            return false;
        }
        UsuarioProyectoPK other = (UsuarioProyectoPK) object;
        if (this.idProyecto != other.idProyecto) {
            return false;
        }
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.UsuarioProyectoPK[ idProyecto=" + idProyecto + ", idUsuario=" + idUsuario + " ]";
    }

}
