package com.example.usuario.gestionproyectos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;

import java.io.Serializable;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MisTareasAdapter extends RecyclerView.Adapter<MisTareasAdapter.ViewHolder> {
    private ArrayList<UserStory> tareas;
    private Context mCtx;
    private Usuario usuario;
    private Proyecto proyecto;
    RestServices restServices;

    public MisTareasAdapter(ArrayList<UserStory> tareas, Context mCtx, Usuario usuario, Proyecto proyecto) {
        this.tareas = tareas;
        this.mCtx = mCtx;
        this.usuario = usuario;
        this.proyecto = proyecto;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tareas, parent, false);

        restServices = ApiUtils.getRestService();

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final UserStory tarea = tareas.get(position);

        holder.codigo.setText(tarea.getCodigoUserStory());
        holder.nombre.setText(tarea.getNombreUs());
        holder.tipo.setText(tarea.getIdTipoUs().getNombreTipoUs());
        holder.descripcion.setText(tarea.getDescripcionUserStory());
        int tipoPrioridad =  tarea.getPrioridadUserStory();
        switch (tipoPrioridad) {
            case 1:
                holder.prioridad.setImageResource(R.color.colorPrioridadAlta);
                break;
            case 2:
                holder.prioridad.setImageResource(R.color.colorPrioridadMedia);
                break;
            case 3:
                holder.prioridad.setImageResource(R.color.colorPrioridadBaja);
                break;
        }
        if(usuario.getIsSuperUser()==1) {
            holder.modificar_eliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupMenu popup = new PopupMenu(mCtx, holder.modificar_eliminar);
                    popup.inflate(R.menu.menu_modificar_eliminar);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.modificar:
                                    Intent myIntent = new Intent(mCtx, TareaUpd.class);
                                    myIntent.putExtra("tarea", (Serializable) tarea);
                                    mCtx.startActivity(myIntent);
                                    return true;
                                case R.id.eliminar:
                                    AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                                    builder.setCancelable(true);
                                    builder.setTitle("¿Está seguro?");
                                    builder.setMessage("Se eliminará la tarea.");
                                    builder.setPositiveButton("Aceptar",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    UserStory theRemovedItem = tareas.get(position);
                                                    // remove your item from data base
                                                    eliminarUS(tarea.getCodigoUserStory());
                                                    tareas.remove(position);  // remove the item from list
                                                    notifyItemRemoved(position); // notify the adapter about the removed item
                                                }
                                            });
                                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });

                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();
                }
            });
            holder.cardTarea.setClickable(false);
        }else{
            holder.modificar_eliminar.setVisibility(View.INVISIBLE);
            holder.cardTarea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent myIntent = new Intent(mCtx, TareaUpd.class);
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mCtx.startActivity(myIntent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (tareas != null) {
            return tareas.size();
        } else {
            return 0;
        }
    }

    public static class ViewHolder extends  RecyclerView.ViewHolder {
        public final View view;
        public final TextView codigo;
        public final TextView nombre;
        public final TextView tipo;
        public final TextView descripcion;
        public final ImageView prioridad;
        public final ImageButton modificar_eliminar;
        public final CardView cardTarea;


        public ViewHolder(View view) {
            super(view);
            this.view = view;
            codigo = view.findViewById(R.id.list_codigo_mis_tareas);
            nombre = view.findViewById(R.id.list_nombre_mis_tareas);
            tipo = view.findViewById(R.id.list_tipo_mis_tareas);
            descripcion = view.findViewById(R.id.list_descripcion_mis_tareas);
            prioridad = view.findViewById(R.id.list_prioridad_mis_tareas);
            modificar_eliminar = view.findViewById(R.id.modificar_eliminar_tarea);
            cardTarea = view.findViewById(R.id.cardTarea);
        }
    }

    public void eliminarUS(String id){
        Call<UserStory> call = restServices.deleteUserStory(id);
        call.enqueue(new Callback<UserStory>(){
            @Override
            public void onResponse(Call<UserStory> call, Response<UserStory> response) {
                if(response.isSuccessful()){
                    Toast.makeText(mCtx, "Tarea eliminada!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserStory> call, Throwable t) {
                Toast.makeText(mCtx,t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}