package com.example.usuario.gestionproyectos;

import java.io.Serializable;

public class EstadoProyecto implements Serializable{

    private static final long serialVersionUID = 1L;

    private Integer idEstadoProyecto;

    private String nombreEstadoProyecto;

    private String descripcionEstadoProyecto;

    public EstadoProyecto() {
    }

    public EstadoProyecto(Integer idEstadoProyecto) {
        this.idEstadoProyecto = idEstadoProyecto;
    }

    public EstadoProyecto(Integer idEstadoProyecto, String nombreEstadoProyecto, String descripcionEstadoProyecto) {
        this.idEstadoProyecto = idEstadoProyecto;
        this.nombreEstadoProyecto = nombreEstadoProyecto;
        this.descripcionEstadoProyecto = descripcionEstadoProyecto;
    }

    public Integer getIdEstadoProyecto() {
        return idEstadoProyecto;
    }

    public void setIdEstadoProyecto(Integer idEstadoProyecto) {
        this.idEstadoProyecto = idEstadoProyecto;
    }

    public String getNombreEstadoProyecto() {
        return nombreEstadoProyecto;
    }

    public void setNombreEstadoProyecto(String nombreEstadoProyecto) {
        this.nombreEstadoProyecto = nombreEstadoProyecto;
    }

    public String getDescripcionEstadoProyecto() {
        return descripcionEstadoProyecto;
    }

    public void setDescripcionEstadoProyecto(String descripcionEstadoProyecto) {
        this.descripcionEstadoProyecto = descripcionEstadoProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstadoProyecto != null ? idEstadoProyecto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoProyecto)) {
            return false;
        }
        EstadoProyecto other = (EstadoProyecto) object;
        if ((this.idEstadoProyecto == null && other.idEstadoProyecto != null) || (this.idEstadoProyecto != null && !this.idEstadoProyecto.equals(other.idEstadoProyecto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.EstadoProyecto[ idEstadoProyecto=" + idEstadoProyecto + " ]";
    }

}
