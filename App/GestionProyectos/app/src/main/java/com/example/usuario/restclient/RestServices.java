package com.example.usuario.restclient;
import com.example.usuario.gestionproyectos.EstadoUs;
import com.example.usuario.gestionproyectos.MisTareas;
import com.example.usuario.gestionproyectos.Proyecto;
import com.example.usuario.gestionproyectos.Rol;
import com.example.usuario.gestionproyectos.Sprint;
import com.example.usuario.gestionproyectos.Tarea;
import com.example.usuario.gestionproyectos.TipoUs;
import com.example.usuario.gestionproyectos.UserStory;
import com.example.usuario.gestionproyectos.Usuario;
import com.example.usuario.gestionproyectos.UsuarioProyecto;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RestServices {

    @POST("usuarios/login")
    Call<Usuario> login(@Body Usuario usuario);

    @GET("usuarios/")
    Call<ArrayList<Usuario>> getUsuarios();

    @POST("usuarios/")
    Call<Usuario> addUser(@Body Usuario usuario);

    @PUT("usuarios/{id}")
    Call<Usuario> updateUser(@Path("id") int id, @Body Usuario usuario);

    @DELETE("usuarios/{id}")
    Call<Usuario> deleteUser(@Path("id") int d);

    @POST("proyectos/{id}")
    Call<Proyecto> deleteProyecto(@Path("id") int d);

    @GET("proyectos/")
    Call<ArrayList<Proyecto>> getProyectos();

    @GET("estadosus/")
    Call<ArrayList<EstadoUs>> getEstadoUS();

    @POST("proyectos/")
    Call<Proyecto> addProyecto(@Body Proyecto proyecto);

    @GET("rol/")
    Call<ArrayList<Rol>> getRoles();

    @GET("sprints/")
    Call<ArrayList<Sprint>> getSprints();

    @GET("sprints/")
    Call<ArrayList<UserStory>> getTareasPorSprint();

    @POST("sprints/")
    Call<Sprint> addSprint(@Body Sprint sprint);

    @GET("sprints/")
    Call<ArrayList<UserStory>> getTareasSinSprint();

    @GET("tipous/")
    Call<ArrayList<TipoUs>> getTiposPorProyecto(@Path("proyecto") int d);

    @GET("usuarios/")
    Call<ArrayList<Usuario>> getUsuariosPorProyecto(@Path("proyecto") int d);

    @POST("userstories/")
    Call<UserStory> addUserStory(@Body UserStory us);

    @POST("userstories/{id}")
    Call<UserStory> deleteUserStory(@Path("id") String d);
}
