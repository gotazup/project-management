package com.example.usuario.gestionproyectos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsuarioUpd extends AppCompatActivity {

    TextView username, email, pass, confirm_pass;
    CheckBox superuser;
    RestServices restServices;
    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_upd);
        restServices = ApiUtils.getRestService();

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("usuario");
        cargar(usuario);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_guardar:
                guardar();
                return true;
            case android.R.id.home:
                Intent usuariosIntent = new Intent(getApplicationContext(), UsuarioList.class);
                usuariosIntent.putExtra("usuario", usuario);
                startActivity(usuariosIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void cargar(Usuario usuario){
        username = (TextView) findViewById(R.id.upd_username);
        username.setText(usuario.getNombreUsuario());
        email = (TextView) findViewById(R.id.upd_email);
        email.setText(usuario.getCorreoUsuario());
        superuser = (CheckBox) findViewById(R.id.upd_superuser);
        if(usuario.getIsSuperUser() == 1){
            superuser.setChecked(true);
        }
    }

    public void guardar(){
        username = (TextView) findViewById(R.id.upd_username);
        email = (TextView) findViewById(R.id.upd_email);
        superuser = (CheckBox) findViewById(R.id.upd_superuser);

        usuario.setNombreUsuario(username.getText().toString());
        usuario.setCorreoUsuario(email.getText().toString());
        if(superuser.isChecked()) {
            usuario.setIsSuperUser(1);
        }else{
            usuario.setIsSuperUser(0);
        }
        Call<Usuario> call = restServices.updateUser(usuario.getIdUsuario(), usuario);
        call.enqueue(new Callback<Usuario>(){
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful()){
                    Toast.makeText(UsuarioUpd.this, "Usuario actualizado correctamente!", Toast.LENGTH_SHORT).show();
                    Intent usuariosIntent = new Intent(getApplicationContext(), UsuarioList.class);
                    usuariosIntent.putExtra("usuario", usuario);
                    startActivity(usuariosIntent);
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(UsuarioUpd.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
