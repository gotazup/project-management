package com.example.usuario.gestionproyectos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;

import retrofit2.Response;
import retrofit2.Call;
import retrofit2.Callback;

public class UsuarioAdd extends AppCompatActivity {

    TextView username, email, pass, confirm_pass;
    CheckBox superuser;
    RestServices restServices;
    Usuario usuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_add);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("usuario");

        restServices = ApiUtils.getRestService();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_guardar:
                guardar();
                return true;
            case android.R.id.home:
                Intent usuariosIntent = new Intent(getApplicationContext(), UsuarioList.class);
                usuariosIntent.putExtra("usuario", usuario);
                startActivity(usuariosIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void guardar(){
        username = (TextView) findViewById(R.id.add_username);
        email = (TextView) findViewById(R.id.add_email);
        pass = (TextView) findViewById(R.id.add_password);
        confirm_pass = (TextView) findViewById(R.id.add_password2);
        superuser = (CheckBox) findViewById(R.id.add_superuser);

        final Usuario usuario = new Usuario(username.getText().toString(), email.getText().toString(), pass.getText().toString());
        usuario.setEstadoUsuario("AC");
        if(superuser.isChecked()) {
            usuario.setIsSuperUser(1);
        }else{
            usuario.setIsSuperUser(0);
        }
        usuario.setUserStoryCollection(null);
        usuario.setUserStoryCollection1(null);
        usuario.setUsuarioProyectoCollection(null);

        Call<Usuario> call = restServices.addUser(usuario);
        call.enqueue(new Callback<Usuario>(){
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful()){
                    Toast.makeText(UsuarioAdd.this, "Usuario creado correctamente!", Toast.LENGTH_SHORT).show();
                    Intent usuariosIntent = new Intent(getApplicationContext(), UsuarioList.class);
                    usuariosIntent.putExtra("usuario", usuario);
                    startActivity(usuariosIntent);
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(UsuarioAdd.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
