package com.example.usuario.gestionproyectos;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MisTareas extends Base {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private Usuario usuario;
    private Proyecto proyecto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("usuario");
        proyecto = (Proyecto) extras.getSerializable("proyecto");

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_mis_tareas, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);

        if(usuario.getIsSuperUser()==0){
            navigationView.getMenu().getItem(2).setVisible(false);
            navigationView.getMenu().getItem(3).setVisible(false);
            navigationView.getMenu().getItem(4).setVisible(false);
        }

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_tareas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position){
                case 0:
                    MisTareasPorHacer por_hacer = new MisTareasPorHacer();
                    Bundle args1 = new Bundle();
                    args1.putSerializable("usuario",usuario);
                    por_hacer.setArguments(args1);
                    return por_hacer;
                case 1:
                    MisTareasEnCurso en_curso = new MisTareasEnCurso();
                    Bundle args2 = new Bundle();
                    args2.putSerializable("usuario",usuario);
                    en_curso.setArguments(args2);
                    return en_curso;
                case 2:
                    MisTareasTerminadas terminadas = new MisTareasTerminadas();
                    Bundle args3 = new Bundle();
                    args3.putSerializable("usuario",usuario);
                    terminadas.setArguments(args3);
                    return terminadas;
                default:
                    return null;
            }
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle menu_modificar_eliminar view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_proyecto:
                Intent proyectoIntent = new Intent(getApplicationContext(), ProyectoList.class);
                proyectoIntent.putExtra("usuario", usuario);
                startActivity(proyectoIntent);
                return true;
            case R.id.nav_mis_tareas:
                Intent misTareasIntent = new Intent(getApplicationContext(), MisTareas.class);
                misTareasIntent.putExtra("proyecto", proyecto);
                misTareasIntent.putExtra("usuario", usuario);
                startActivity(misTareasIntent);
                return true;
            case R.id.nav_backlog:
                Intent backlogIntent = new Intent(getApplicationContext(), Backlog.class);
                backlogIntent.putExtra("proyecto", proyecto);
                backlogIntent.putExtra("usuario", usuario);
                startActivity(backlogIntent);
                return true;
            case R.id.nav_sprints:
                Intent sprintsIntent = new Intent(getApplicationContext(), SprintList.class);
                sprintsIntent.putExtra("proyecto", proyecto);
                sprintsIntent.putExtra("usuario", usuario);
                startActivity(sprintsIntent);
                return true;
            case R.id.nav_usuario:
                Intent usuariosIntent = new Intent(getApplicationContext(), UsuarioList.class);
                usuariosIntent.putExtra("usuario", usuario);
                startActivity(usuariosIntent);
                return true;
            default:
                return false;
        }
    }
}
