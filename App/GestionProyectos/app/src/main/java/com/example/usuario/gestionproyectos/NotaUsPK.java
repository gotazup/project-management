package com.example.usuario.gestionproyectos;

import java.io.Serializable;

public class NotaUsPK implements Serializable {

    private int idNotaUs;

    private int idUserStory;

    private int idProyecto;

    public NotaUsPK() {
    }

    public NotaUsPK(int idNotaUs, int idUserStory, int idProyecto) {
        this.idNotaUs = idNotaUs;
        this.idUserStory = idUserStory;
        this.idProyecto = idProyecto;
    }

    public int getIdNotaUs() {
        return idNotaUs;
    }

    public void setIdNotaUs(int idNotaUs) {
        this.idNotaUs = idNotaUs;
    }

    public int getIdUserStory() {
        return idUserStory;
    }

    public void setIdUserStory(int idUserStory) {
        this.idUserStory = idUserStory;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idNotaUs;
        hash += (int) idUserStory;
        hash += (int) idProyecto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaUsPK)) {
            return false;
        }
        NotaUsPK other = (NotaUsPK) object;
        if (this.idNotaUs != other.idNotaUs) {
            return false;
        }
        if (this.idUserStory != other.idUserStory) {
            return false;
        }
        if (this.idProyecto != other.idProyecto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.NotaUsPK[ idNotaUs=" + idNotaUs + ", idUserStory=" + idUserStory + ", idProyecto=" + idProyecto + " ]";
    }

}
