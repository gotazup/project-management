package com.example.usuario.gestionproyectos;

import java.io.Serializable;
import java.util.Date;

public class NotaUs implements Serializable {

    private static final long serialVersionUID = 1L;

    protected NotaUsPK notaUsPK;

    private String descripcionNota;

    private long fechaCreacionNota;

    private Usuario idUsuarioCreacionNota;

    private UserStory userStory;

    public NotaUs() {
    }

    public NotaUs(NotaUsPK notaUsPK) {
        this.notaUsPK = notaUsPK;
    }

    public NotaUs(NotaUsPK notaUsPK, String descripcionNota, long fechaCreacionNota, Usuario idUsuarioCreacionNota) {
        this.notaUsPK = notaUsPK;
        this.descripcionNota = descripcionNota;
        this.fechaCreacionNota = fechaCreacionNota;
        this.idUsuarioCreacionNota = idUsuarioCreacionNota;
    }

    public NotaUs(String descripcionNota, long fechaCreacionNota, Usuario idUsuarioCreacionNota) {
        this.descripcionNota = descripcionNota;
        this.fechaCreacionNota = fechaCreacionNota;
        this.idUsuarioCreacionNota = idUsuarioCreacionNota;
    }

    public NotaUs(int idNotaUs, int idUserStory, int idProyecto) {
        this.notaUsPK = new NotaUsPK(idNotaUs, idUserStory, idProyecto);
    }

    public NotaUsPK getNotaUsPK() {
        return notaUsPK;
    }

    public void setNotaUsPK(NotaUsPK notaUsPK) {
        this.notaUsPK = notaUsPK;
    }

    public String getDescripcionNota() {
        return descripcionNota;
    }

    public void setDescripcionNota(String descripcionNota) {
        this.descripcionNota = descripcionNota;
    }

    public long getFechaCreacionNota() {
        return fechaCreacionNota;
    }

    public void setFechaCreacionNota(long fechaCreacionNota) {
        this.fechaCreacionNota = fechaCreacionNota;
    }

    public Usuario getIdUsuarioCreacionNota() {
        return idUsuarioCreacionNota;
    }

    public void setIdUsuarioCreacionNota(Usuario idUsuarioCreacionNota) {
        this.idUsuarioCreacionNota = idUsuarioCreacionNota;
    }

    public UserStory getUserStory() {
        return userStory;
    }

    public void setUserStory(UserStory userStory) {
        this.userStory = userStory;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notaUsPK != null ? notaUsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaUs)) {
            return false;
        }
        NotaUs other = (NotaUs) object;
        if ((this.notaUsPK == null && other.notaUsPK != null) || (this.notaUsPK != null && !this.notaUsPK.equals(other.notaUsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.NotaUs[ notaUsPK=" + notaUsPK + " ]";
    }

}
