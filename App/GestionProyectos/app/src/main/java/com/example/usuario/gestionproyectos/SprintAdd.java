package com.example.usuario.gestionproyectos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usuario.restclient.ApiUtils;
import com.example.usuario.restclient.RestServices;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SprintAdd extends AppCompatActivity {

    private RecyclerView tareas;
    private RecyclerView.Adapter adapter;
    private TextView codigo, nombre, fecha_inicio, fecha_fin, estado;
    ArrayList<UserStory> listTareas = new ArrayList<UserStory>();
    RestServices restServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sprint_add);
        restServices = ApiUtils.getRestService();

        initTareas();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_guardar:
                guardar();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initTareas(){
        Call<ArrayList<UserStory>> call = restServices.getTareasSinSprint();
        call.enqueue(new Callback<ArrayList<UserStory>>(){
            @Override
            public void onResponse(Call<ArrayList<UserStory>> call, Response<ArrayList<UserStory>> response) {
                RecyclerView tareas;
                RecyclerView.Adapter adapter;
                if(response.isSuccessful()){
                    listTareas = response.body();

                    tareas = (RecyclerView) findViewById(R.id.tareas_sprint);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SprintAdd.this);
                    tareas.setLayoutManager(mLayoutManager);

                    adapter = new SprintAddAdapter(listTareas, SprintAdd.this);
                    tareas.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserStory>> call, Throwable t) {
                Toast.makeText(SprintAdd.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void guardar(){
        codigo = (TextView) findViewById(R.id.det_codigo_sprint);
        nombre = (TextView) findViewById(R.id.det_nombre_sprint);
        fecha_inicio = (TextView) findViewById(R.id.det_fecha_inicio_sprint);
        fecha_fin = (TextView) findViewById(R.id.det_fecha_fin_sprint);

        long startDate=0;
        long endDate=0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String startDateString = fecha_inicio.getText().toString();
            String endDateString = fecha_fin.getText().toString();
            Date date1 = sdf.parse(startDateString);
            Date date2 = sdf.parse(endDateString);

            startDate = date1.getTime()/1000;
            endDate = date2.getTime()/1000;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Sprint sprint = new Sprint(codigo.getText().toString(),nombre.getText().toString(),startDate,endDate);


    }
}
