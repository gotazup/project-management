package com.example.usuario.restclient;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class JsonDateDeserializer implements JsonDeserializer<Date> {
    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String date = json.getAsString();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyy");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC-4"));

        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            System.out.println("Failed to parse Date due to:");
            return null;
        }
    }
}
