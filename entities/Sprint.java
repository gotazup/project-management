/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gustavo
 */
@Entity
@Table(name = "sprint")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sprint.findAll", query = "SELECT s FROM Sprint s")
    , @NamedQuery(name = "Sprint.findByIdSprint", query = "SELECT s FROM Sprint s WHERE s.sprintPK.idSprint = :idSprint")
    , @NamedQuery(name = "Sprint.findByIdProyecto", query = "SELECT s FROM Sprint s WHERE s.sprintPK.idProyecto = :idProyecto")
    , @NamedQuery(name = "Sprint.findByNombreSprint", query = "SELECT s FROM Sprint s WHERE s.nombreSprint = :nombreSprint")
    , @NamedQuery(name = "Sprint.findByEstadoSprint", query = "SELECT s FROM Sprint s WHERE s.estadoSprint = :estadoSprint")
    , @NamedQuery(name = "Sprint.findByFechaCreacionSprint", query = "SELECT s FROM Sprint s WHERE s.fechaCreacionSprint = :fechaCreacionSprint")
    , @NamedQuery(name = "Sprint.findByFechaFinSprint", query = "SELECT s FROM Sprint s WHERE s.fechaFinSprint = :fechaFinSprint")})
public class Sprint implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SprintPK sprintPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nombre_sprint")
    private String nombreSprint;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "estado_sprint")
    private String estadoSprint;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion_sprint")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionSprint;
    @Column(name = "fecha_fin_sprint")
    @Temporal(TemporalType.DATE)
    private Date fechaFinSprint;
    
    @Column(name="codigo_sprint")
    private String codigoSprint;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sprint")
    private Collection<UserStory> userStoryCollection;
   
    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto", insertable = false, updatable = false)
    @ManyToOne(fetch= FetchType.LAZY)
    private Proyecto proyecto;
   
    public Sprint() {
    }

    public Sprint(SprintPK sprintPK) {
        this.sprintPK = sprintPK;
    }

    public Sprint(SprintPK sprintPK, String nombreSprint, String estadoSprint, Date fechaCreacionSprint) {
        this.sprintPK = sprintPK;
        this.nombreSprint = nombreSprint;
        this.estadoSprint = estadoSprint;
        this.fechaCreacionSprint = fechaCreacionSprint;
    }

    public Sprint(int idSprint, int idProyecto) {
        this.sprintPK = new SprintPK(idSprint, idProyecto);
    }

    public SprintPK getSprintPK() {
        return sprintPK;
    }

    public void setSprintPK(SprintPK sprintPK) {
        this.sprintPK = sprintPK;
    }

    public String getNombreSprint() {
        return nombreSprint;
    }

    public void setNombreSprint(String nombreSprint) {
        this.nombreSprint = nombreSprint;
    }

    public String getEstadoSprint() {
        return estadoSprint;
    }

    public void setEstadoSprint(String estadoSprint) {
        this.estadoSprint = estadoSprint;
    }

    public Date getFechaCreacionSprint() {
        return fechaCreacionSprint;
    }

    public void setFechaCreacionSprint(Date fechaCreacionSprint) {
        this.fechaCreacionSprint = fechaCreacionSprint;
    }

    public Date getFechaFinSprint() {
        return fechaFinSprint;
    }

    public void setFechaFinSprint(Date fechaFinSprint) {
        this.fechaFinSprint = fechaFinSprint;
    }

    @XmlTransient
    public Collection<UserStory> getUserStoryCollection() {
        return userStoryCollection;
    }

    public void setUserStoryCollection(Collection<UserStory> userStoryCollection) {
        this.userStoryCollection = userStoryCollection;
    }
    
    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }
    
    public String getCodigoSprint() {
        return codigoSprint;
    }

    public void setCodigoSprint(String codigoSprint) {
        this.codigoSprint = codigoSprint;
    }
    
    

    /*@Override
    public int hashCode() {
        int hash = 0;
        hash += (sprintPK != null ? sprintPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sprint)) {
            return false;
        }
        Sprint other = (Sprint) object;
        if ((this.sprintPK == null && other.sprintPK != null) || (this.sprintPK != null && !this.sprintPK.equals(other.sprintPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.Sprint[ sprintPK=" + sprintPK + " ]";
    }*/
    
}
