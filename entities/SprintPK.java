/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author gustavo
 */
@Embeddable
public class SprintPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_sprint")
    private int idSprint;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_proyecto")
    private int idProyecto;

    public SprintPK() {
    }

    public SprintPK(int idSprint, int idProyecto) {
        this.idSprint = idSprint;
        this.idProyecto = idProyecto;
    }

    public int getIdSprint() {
        return idSprint;
    }

    public void setIdSprint(int idSprint) {
        this.idSprint = idSprint;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }
    /*
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idSprint;
        hash += (int) idProyecto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SprintPK)) {
            return false;
        }
        SprintPK other = (SprintPK) object;
        if (this.idSprint != other.idSprint) {
            return false;
        }
        if (this.idProyecto != other.idProyecto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.SprintPK[ idSprint=" + idSprint + ", idProyecto=" + idProyecto + " ]";
    }*/
    
}
