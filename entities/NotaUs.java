/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gustavo
 */
@Entity
@Table(name = "nota_us")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaUs.findAll", query = "SELECT n FROM NotaUs n")
    , @NamedQuery(name = "NotaUs.findByIdNotaUs", query = "SELECT n FROM NotaUs n WHERE n.notaUsPK.idNotaUs = :idNotaUs")
    , @NamedQuery(name = "NotaUs.findByIdUserStory", query = "SELECT n FROM NotaUs n WHERE n.notaUsPK.idUserStory = :idUserStory")
    , @NamedQuery(name = "NotaUs.findByIdProyecto", query = "SELECT n FROM NotaUs n WHERE n.notaUsPK.idProyecto = :idProyecto")
    , @NamedQuery(name = "NotaUs.findByDescripcionNota", query = "SELECT n FROM NotaUs n WHERE n.descripcionNota = :descripcionNota")
    , @NamedQuery(name = "NotaUs.findByFechaCreacionNota", query = "SELECT n FROM NotaUs n WHERE n.fechaCreacionNota = :fechaCreacionNota")
    , @NamedQuery(name = "NotaUs.findByIdUsuarioCreacionNota", query = "SELECT n FROM NotaUs n WHERE n.idUsuarioCreacionNota = :idUsuarioCreacionNota")})
public class NotaUs implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NotaUsPK notaUsPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descripcion_nota")
    private String descripcionNota;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion_nota")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacionNota;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_usuario_creacion_nota")
    private int idUsuarioCreacionNota;
    @JoinColumns({
        @JoinColumn(name = "id_user_story", referencedColumnName = "id_user_story", insertable = false, updatable = false)
        , @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private UserStory userStory;

    public NotaUs() {
    }

    public NotaUs(NotaUsPK notaUsPK) {
        this.notaUsPK = notaUsPK;
    }

    public NotaUs(NotaUsPK notaUsPK, String descripcionNota, Date fechaCreacionNota, int idUsuarioCreacionNota) {
        this.notaUsPK = notaUsPK;
        this.descripcionNota = descripcionNota;
        this.fechaCreacionNota = fechaCreacionNota;
        this.idUsuarioCreacionNota = idUsuarioCreacionNota;
    }

    public NotaUs(int idNotaUs, int idUserStory, int idProyecto) {
        this.notaUsPK = new NotaUsPK(idNotaUs, idUserStory, idProyecto);
    }

    public NotaUsPK getNotaUsPK() {
        return notaUsPK;
    }

    public void setNotaUsPK(NotaUsPK notaUsPK) {
        this.notaUsPK = notaUsPK;
    }

    public String getDescripcionNota() {
        return descripcionNota;
    }

    public void setDescripcionNota(String descripcionNota) {
        this.descripcionNota = descripcionNota;
    }

    public Date getFechaCreacionNota() {
        return fechaCreacionNota;
    }

    public void setFechaCreacionNota(Date fechaCreacionNota) {
        this.fechaCreacionNota = fechaCreacionNota;
    }

    public int getIdUsuarioCreacionNota() {
        return idUsuarioCreacionNota;
    }

    public void setIdUsuarioCreacionNota(int idUsuarioCreacionNota) {
        this.idUsuarioCreacionNota = idUsuarioCreacionNota;
    }

    public UserStory getUserStory() {
        return userStory;
    }

    public void setUserStory(UserStory userStory) {
        this.userStory = userStory;
    }
    /*
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notaUsPK != null ? notaUsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaUs)) {
            return false;
        }
        NotaUs other = (NotaUs) object;
        if ((this.notaUsPK == null && other.notaUsPK != null) || (this.notaUsPK != null && !this.notaUsPK.equals(other.notaUsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.NotaUs[ notaUsPK=" + notaUsPK + " ]";
    }
    */
}
