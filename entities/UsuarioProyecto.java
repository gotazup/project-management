/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gustavo
 */
@Entity
@Table(name = "usuario_proyecto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioProyecto.findAll", query = "SELECT u FROM UsuarioProyecto u")
    , @NamedQuery(name = "UsuarioProyecto.findByIdProyecto", query = "SELECT u FROM UsuarioProyecto u WHERE u.usuarioProyectoPK.idProyecto = :idProyecto")
    , @NamedQuery(name = "UsuarioProyecto.findByIdUsuario", query = "SELECT u FROM UsuarioProyecto u WHERE u.usuarioProyectoPK.idUsuario = :idUsuario")})
public class UsuarioProyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuarioProyectoPK usuarioProyectoPK;
    /*@JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Proyecto proyecto1;*/
    @JoinColumn(name = "id_rol", referencedColumnName = "id_rol")
    @ManyToOne(optional = false)
    private Rol idRol;
    /*@JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;*/

    public UsuarioProyecto() {
    }

    public UsuarioProyecto(UsuarioProyectoPK usuarioProyectoPK) {
        this.usuarioProyectoPK = usuarioProyectoPK;
    }


    public UsuarioProyecto(int idProyecto, int idUsuario) {
        this.usuarioProyectoPK = new UsuarioProyectoPK(idProyecto, idUsuario);
    }

    public UsuarioProyectoPK getUsuarioProyectoPK() {
        return usuarioProyectoPK;
    }

    public void setUsuarioProyectoPK(UsuarioProyectoPK usuarioProyectoPK) {
        this.usuarioProyectoPK = usuarioProyectoPK;
    }
/*
    public void setProyecto1(Proyecto proyecto1) {
        this.proyecto1 = proyecto1;
    }
*/
    public Rol getIdRol() {
        return idRol;
    }

    public void setIdRol(Rol idRol) {
        this.idRol = idRol;
    }
/*
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }*/
    /*
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioProyectoPK != null ? usuarioProyectoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioProyecto)) {
            return false;
        }
        UsuarioProyecto other = (UsuarioProyecto) object;
        if ((this.usuarioProyectoPK == null && other.usuarioProyectoPK != null) || (this.usuarioProyectoPK != null && !this.usuarioProyectoPK.equals(other.usuarioProyectoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.UsuarioProyecto[ usuarioProyectoPK=" + usuarioProyectoPK + " ]";
    }
    */
}
