/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.services;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import org.pol.una.py.entities.UsuarioProyecto;
import org.pol.una.py.entities.UsuarioProyectoPK;

/**
 *
 * @author gustavo
 */
@Stateless
@Path("usuariosproyecto")
public class UsuarioProyectoFacadeREST extends AbstractFacade<UsuarioProyecto> {

    @PersistenceContext(unitName = "WebApplication1PU")
    private EntityManager em;

    private UsuarioProyectoPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idProyecto=idProyectoValue;idUsuario=idUsuarioValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        org.pol.una.py.entities.UsuarioProyectoPK key = new org.pol.una.py.entities.UsuarioProyectoPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idProyecto = map.get("idProyecto");
        if (idProyecto != null && !idProyecto.isEmpty()) {
            key.setIdProyecto(new java.lang.Integer(idProyecto.get(0)));
        }
        java.util.List<String> idUsuario = map.get("idUsuario");
        if (idUsuario != null && !idUsuario.isEmpty()) {
            key.setIdUsuario(new java.lang.Integer(idUsuario.get(0)));
        }
        return key;
    }

    public UsuarioProyectoFacadeREST() {
        super(UsuarioProyecto.class);
    }

    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(UsuarioProyecto entity) {
        super.create(entity);
    }

    @PUT
    @Path("{idUsuario}/{idProyecto}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("idUsuario") int idUsuario, @PathParam("idProyecto") int idProyecto, UsuarioProyecto entity) {
        UsuarioProyectoPK key = new UsuarioProyectoPK();
        key.setIdUsuario(idUsuario);
        key.setIdProyecto(idProyecto);
        super.edit(entity);
    }

    @DELETE
    @Path("{idUsuario}/{idProyecto}")
    public void remove(@PathParam("idUsuario") int idUsuario, @PathParam("idProyecto") int idProyecto) {
        UsuarioProyectoPK key = new UsuarioProyectoPK();
        key.setIdUsuario(idUsuario);
        key.setIdProyecto(idProyecto);
        super.remove(super.find(key));
    }

    @GET
    @Path("{idUsuario}/{idProyecto}")
    @Produces(MediaType.APPLICATION_JSON)
    public UsuarioProyecto find(@PathParam("idUsuario") int idUsuario, @PathParam("idProyecto") int idProyecto) {
        UsuarioProyectoPK key = new UsuarioProyectoPK();
        key.setIdUsuario(idUsuario);
        key.setIdProyecto(idProyecto);
        return super.find(key);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<UsuarioProyecto> findAll() {
        return super.findAll();
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
