/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.services;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import org.pol.una.py.entities.UserStory;
import org.pol.una.py.entities.UserStoryPK;
import org.pol.una.py.entities.Usuario;

/**
 *
 * @author gustavo
 */
@Stateless
@Path("userstories")
public class UserStoryFacadeREST extends AbstractFacade<UserStory> {

    @PersistenceContext(unitName = "WebApplication1PU")
    private EntityManager em;

    private UserStoryPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idUserStory=idUserStoryValue;idProyecto=idProyectoValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        org.pol.una.py.entities.UserStoryPK key = new org.pol.una.py.entities.UserStoryPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idUserStory = map.get("idUserStory");
        if (idUserStory != null && !idUserStory.isEmpty()) {
            key.setIdUserStory(new java.lang.Integer(idUserStory.get(0)));
        }
        java.util.List<String> idProyecto = map.get("idProyecto");
        if (idProyecto != null && !idProyecto.isEmpty()) {
            key.setIdProyecto(new java.lang.Integer(idProyecto.get(0)));
        }
        return key;
    }

    public UserStoryFacadeREST() {
        super(UserStory.class);
    }

    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(UserStory entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") PathSegment id, UserStory entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        org.pol.una.py.entities.UserStoryPK key = getPrimaryKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserStory find(@PathParam("id") PathSegment id) {
        org.pol.una.py.entities.UserStoryPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserStory> findAll() {
        return super.findAll();
    }
    
    @GET
    @Path("proyectos/{idProyecto}/{idUsuarioAsignadoUserStory}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserStory> findByProyecto(@PathParam("idUsuarioAsignadoUserStory") Integer idUsuarioAsignadoUserStory,@PathParam("idProyecto") Integer idProyecto) {
        Query query = getEntityManager().createNamedQuery("UserStory.findByIdUsuario");
        query.setParameter("idUsuarioAsignadoUserStory", idUsuarioAsignadoUserStory);
        query.setParameter("idProyecto", idProyecto);
        List<UserStory> listaUsuarios = query.getResultList();
        if( !listaUsuarios.isEmpty() ){
            return listaUsuarios;
        }
        return null;
    }

 

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    @GET
    @Path("proyectos/{idProyecto}/sprint/{idSprint}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserStory> findBySprint(@PathParam("idProyecto") int idProyecto, @PathParam("idSprint") int idSprint) {
        Query query = getEntityManager().createNamedQuery("UserStory.findBySprintNumber");
        query.setParameter("idProyecto", idProyecto);
        query.setParameter("idSprint", idSprint);
        List<UserStory> listaUs = query.getResultList();
        if( !listaUs.isEmpty() ){
            return listaUs;
        }
        return null;
    }
    
    @GET
    @Path("backlog/{idProyecto}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserStory> findBacklog(@PathParam("idProyecto") int idProyecto) {
        Query query = getEntityManager().createNamedQuery("UserStory.findBacklog");
        query.setParameter("idProyecto", idProyecto);
        List<UserStory> listaUs = query.getResultList();
        if( !listaUs.isEmpty() ){
            return listaUs;
        }
        return null;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
