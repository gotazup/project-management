/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author gustavo
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(org.pol.una.py.services.EstadoProyectoFacadeREST.class);
        resources.add(org.pol.una.py.services.EstadoUsFacadeREST.class);
        resources.add(org.pol.una.py.services.NotaUsFacadeREST.class);
        resources.add(org.pol.una.py.services.ProyectoFacadeREST.class);
        resources.add(org.pol.una.py.services.RolFacadeREST.class);
        resources.add(org.pol.una.py.services.SprintFacadeREST.class);
        resources.add(org.pol.una.py.services.TipoUsFacadeREST.class);
        resources.add(org.pol.una.py.services.UserStoryFacadeREST.class);
        resources.add(org.pol.una.py.services.UsuarioFacadeREST.class);
        resources.add(org.pol.una.py.services.UsuarioProyectoFacadeREST.class);
    }
    
}
