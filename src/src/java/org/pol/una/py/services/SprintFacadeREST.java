/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.services;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import org.pol.una.py.entities.Sprint;
import org.pol.una.py.entities.SprintPK;

/**
 *
 * @author gustavo
 */
@Stateless
@Path("sprints")
public class SprintFacadeREST extends AbstractFacade<Sprint> {

    @PersistenceContext(unitName = "WebApplication1PU")
    private EntityManager em;

    private SprintPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idSprint=idSprintValue;idProyecto=idProyectoValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        org.pol.una.py.entities.SprintPK key = new org.pol.una.py.entities.SprintPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idSprint = map.get("idSprint");
        if (idSprint != null && !idSprint.isEmpty()) {
            key.setIdSprint(new java.lang.Integer(idSprint.get(0)));
        }
        java.util.List<String> idProyecto = map.get("idProyecto");
        if (idProyecto != null && !idProyecto.isEmpty()) {
            key.setIdProyecto(new java.lang.Integer(idProyecto.get(0)));
        }
        return key;
    }

    public SprintFacadeREST() {
        super(Sprint.class);
    }

    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Sprint entity) {
        super.create(entity);
    }

    @PUT
    @Path("{idSprint}/{idProyecto}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("idSprint") int idSprint, @PathParam("idProyecto") int idProyecto, Sprint entity) {
        SprintPK key = new SprintPK();
        key.setIdSprint(idSprint);
        key.setIdProyecto(idProyecto);
        super.edit(entity);
    }

    @DELETE
    @Path("{idSprint}/{idProyecto}")
    public void remove(@PathParam("idSprint") int idSprint, @PathParam("idProyecto") int idProyecto) {
        SprintPK key = new SprintPK();
        key.setIdSprint(idSprint);
        key.setIdProyecto(idProyecto);
        super.remove(super.find(key));
    }

    @GET
    @Path("{idSprint}/{idProyecto}")
    @Produces(MediaType.APPLICATION_JSON)
    public Sprint find(@PathParam("idSprint") int idSprint, @PathParam("idProyecto") int idProyecto) {
        SprintPK key = new SprintPK();
        key.setIdSprint(idSprint);
        key.setIdProyecto(idProyecto);
        return super.find(key);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Sprint> findAll() {
        return super.findAll();
    }


    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
