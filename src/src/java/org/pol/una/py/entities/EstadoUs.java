/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gustavo
 */
@Entity
@Table(name = "estado_us")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadoUs.findAll", query = "SELECT e FROM EstadoUs e")
    , @NamedQuery(name = "EstadoUs.findByIdEstadoUs", query = "SELECT e FROM EstadoUs e WHERE e.idEstadoUs = :idEstadoUs")
    , @NamedQuery(name = "EstadoUs.findByDescripcionEstado", query = "SELECT e FROM EstadoUs e WHERE e.descripcionEstado = :descripcionEstado")})
public class EstadoUs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_estado_us")
    private Integer idEstadoUs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "descripcion_estado")
    private String descripcionEstado;
    /*@OneToMany(cascade = CascadeType.ALL, mappedBy = "idEstadoUs")
    private Collection<UserStory> userStoryCollection;
    */
    public EstadoUs() {
    }

    public EstadoUs(Integer idEstadoUs) {
        this.idEstadoUs = idEstadoUs;
    }

    public EstadoUs(Integer idEstadoUs, String descripcionEstado) {
        this.idEstadoUs = idEstadoUs;
        this.descripcionEstado = descripcionEstado;
    }

    public Integer getIdEstadoUs() {
        return idEstadoUs;
    }

    public void setIdEstadoUs(Integer idEstadoUs) {
        this.idEstadoUs = idEstadoUs;
    }

    public String getDescripcionEstado() {
        return descripcionEstado;
    }

    public void setDescripcionEstado(String descripcionEstado) {
        this.descripcionEstado = descripcionEstado;
    }
    /*
    @XmlTransient
    public Collection<UserStory> getUserStoryCollection() {
        return userStoryCollection;
    }

    public void setUserStoryCollection(Collection<UserStory> userStoryCollection) {
        this.userStoryCollection = userStoryCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstadoUs != null ? idEstadoUs.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoUs)) {
            return false;
        }
        EstadoUs other = (EstadoUs) object;
        if ((this.idEstadoUs == null && other.idEstadoUs != null) || (this.idEstadoUs != null && !this.idEstadoUs.equals(other.idEstadoUs))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "prueba.entities.EstadoUs[ idEstadoUs=" + idEstadoUs + " ]";
    }
    */
}
