/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author gustavo
 */
@Entity
@Table(name = "tipo_us")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoUs.findAll", query = "SELECT t FROM TipoUs t")
    , @NamedQuery(name = "TipoUs.findByIdTipoUs", query = "SELECT t FROM TipoUs t WHERE t.idTipoUs = :idTipoUs")
    , @NamedQuery(name = "TipoUs.findByNombreTipoUs", query = "SELECT t FROM TipoUs t WHERE t.nombreTipoUs = :nombreTipoUs")})
    public class TipoUs implements Serializable {

        private static final long serialVersionUID = 1L;
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Basic(optional = false)
        @Column(name = "id_tipo_us")
        private Integer idTipoUs;
        @Basic(optional = false)
        @NotNull
        @Size(min = 1, max = 30)
        @Column(name = "nombre_tipo_us")
        private String nombreTipoUs;
        @ManyToMany(mappedBy = "tipoUsCollection")
        private Collection<Proyecto> proyectoCollection;
        @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoUs")
        private Collection<UserStory> userStoryCollection;

        public TipoUs() {
        }

        public TipoUs(Integer idTipoUs) {
            this.idTipoUs = idTipoUs;
        }

        public TipoUs(Integer idTipoUs, String nombreTipoUs) {
            this.idTipoUs = idTipoUs;
            this.nombreTipoUs = nombreTipoUs;
        }

        public Integer getIdTipoUs() {
            return idTipoUs;
        }

        public void setIdTipoUs(Integer idTipoUs) {
            this.idTipoUs = idTipoUs;
        }

        public String getNombreTipoUs() {
            return nombreTipoUs;
        }

        public void setNombreTipoUs(String nombreTipoUs) {
            this.nombreTipoUs = nombreTipoUs;
        }
        /*
        @XmlTransient
        public Collection<Proyecto> getProyectoCollection() {
            return proyectoCollection;
        }
        */
        public void setProyectoCollection(Collection<Proyecto> proyectoCollection) {
            this.proyectoCollection = proyectoCollection;
        }

        @XmlTransient
        public Collection<UserStory> getUserStoryCollection() {
            return userStoryCollection;
        }

        public void setUserStoryCollection(Collection<UserStory> userStoryCollection) {
            this.userStoryCollection = userStoryCollection;
        }
        /*
        @Override
        public int hashCode() {
            int hash = 0;
            hash += (idTipoUs != null ? idTipoUs.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object object) {
            // TODO: Warning - this method won't work in the case the id fields are not set
            if (!(object instanceof TipoUs)) {
                return false;
            }
            TipoUs other = (TipoUs) object;
            if ((this.idTipoUs == null && other.idTipoUs != null) || (this.idTipoUs != null && !this.idTipoUs.equals(other.idTipoUs))) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "prueba.entities.TipoUs[ idTipoUs=" + idTipoUs + " ]";
        }
        */
    }
