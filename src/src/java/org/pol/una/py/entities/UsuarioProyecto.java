/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pol.una.py.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author gustavo
 */
@Entity
@Table(name = "proyecto_usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioProyecto.findAll", query = "SELECT u FROM UsuarioProyecto u")
    , @NamedQuery(name = "UsuarioProyecto.findById", query = "SELECT u FROM UsuarioProyecto u WHERE u.usuarioProyectoPK = :usuarioProyectoPK")
    , @NamedQuery(name = "UsuarioProyecto.findByIdUsuario", query = "SELECT u FROM UsuarioProyecto u WHERE u.usuario.idUsuario = :idUsuario")})
public class UsuarioProyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_usuario_proyecto")
    protected int usuarioProyectoPK;
    
    @JoinColumn(name= "id_rol")
    @OneToOne
    private Rol rol;
    
    @JoinColumn(name= "id_usuario")
    @OneToOne
    private Usuario usuario;
    
    
    /*@JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;*/

    public UsuarioProyecto() {
    }

    public int getUsuarioProyectoPK() {
        return usuarioProyectoPK;
    }

    public void setUsuarioProyectoPK(int usuarioProyectoPK) {
        this.usuarioProyectoPK = usuarioProyectoPK;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
